/*
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/**
 * Package of Nuiton-validator api.
 *
 * <h2>The <b>Validator</b> api</h2>
 * <p>
 * The {@link io.ultreia.java4all.validation.api.NuitonValidator} is the object responsible
 * to launch validation for a given object and then return the result of
 * validation in a {@link io.ultreia.java4all.validation.api.NuitonValidatorResult} via the
 * method {@link io.ultreia.java4all.validation.api.NuitonValidator#validate(Object, NuitonValidationContext)}.
 * </p>
 *
 * <pre>
 * NuitonValidator&lt;O&gt; validator = XXX;
 * NuitonValidatorResult result = validator.validate(o);
 * </pre>
 *
 * <h2>Obtain a validator</h2>
 * To obtain a validator use the factory of validators : {@link io.ultreia.java4all.validation.api.NuitonValidatorProviders}.
 *
 * <h2>Implements the validator api</h2>
 * <p>
 * At the moment, there is an offered implementation based on xwork2 framework.
 *
 * <strong>To be continued...</strong>
 *
 * @since 2.0
 */
package io.ultreia.java4all.validation.api;

