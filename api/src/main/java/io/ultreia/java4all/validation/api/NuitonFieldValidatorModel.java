package io.ultreia.java4all.validation.api;

/*-
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;

/**
 * Created at 09/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonFieldValidatorModel {

    private final String fieldName;
    private final Set<String> comments;

    public NuitonFieldValidatorModel(String fieldName, Set<String> comments) {
        this.fieldName = fieldName;
        this.comments = comments;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Set<String> getComments() {
        return comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NuitonFieldValidatorModel)) return false;
        NuitonFieldValidatorModel that = (NuitonFieldValidatorModel) o;
        return Objects.equals(fieldName, that.fieldName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldName);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", NuitonFieldValidatorModel.class.getSimpleName() + "[", "]")
                .add("fieldName='" + fieldName + "'")
                .add("comments=" + comments)
                .toString();
    }
}
