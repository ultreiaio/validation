package io.ultreia.java4all.validation.api.io;

/*-
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.util.json.JsonAdapter;
import io.ultreia.java4all.validation.api.NuitonFieldValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorModelEntry;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.lang.reflect.Type;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Created at 06/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonValidatorModelAdapter<O> implements JsonSerializer<NuitonValidatorModel<O>>, JsonDeserializer<NuitonValidatorModel<O>>, JsonAdapter {

    public static final String TYPE = "type";
    public static final String CONTEXT = "context";

    @Override
    public Class<?> type() {
        return Class.class;
    }

    @Override
    public JsonElement serialize(NuitonValidatorModel<O> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        result.add(TYPE, context.serialize(src.getKey().getType()));
        result.addProperty(CONTEXT, src.getKey().getContext());
        for (NuitonValidatorScope scope : src.getScopes()) {
            JsonObject scopeResult = new JsonObject();
            result.add(scope.name(), scopeResult);
            List<NuitonFieldValidatorModel> fieldValidatorModels = src.getFieldsByScope().get(scope);
            for (NuitonFieldValidatorModel fieldValidatorModel : fieldValidatorModels) {
                scopeResult.add(fieldValidatorModel.getFieldName(), context.serialize(fieldValidatorModel.getComments()));
            }
        }
        return result;
    }

    @Override
    public NuitonValidatorModel<O> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        Class<O> type = context.deserialize(object.get(TYPE), Class.class);
        String keyContext = Optional.ofNullable(object.get(CONTEXT)).map(JsonElement::getAsString).orElse(null);
        Map<NuitonValidatorScope, List<NuitonFieldValidatorModel>> fields = new TreeMap<>();
        for (NuitonValidatorScope scope : NuitonValidatorScope.values()) {
            Optional.ofNullable(object.get(scope.name())).map(JsonElement::getAsJsonObject).ifPresent(f -> {
                Map<String, TreeSet<String>> scopeFields = context.deserialize(f, new TypeToken<Map<String, TreeSet<String>>>() {
                }.getType());
                List<NuitonFieldValidatorModel> fieldModels = scopeFields
                        .entrySet()
                        .stream().map(e -> new NuitonFieldValidatorModel(e.getKey(), e.getValue()))
                        .sorted(Comparator.comparing(NuitonFieldValidatorModel::getFieldName))
                        .collect(Collectors.toList());
                fields.put(scope, fieldModels);
            });
        }
        Set<NuitonValidatorScope> scopes = fields.keySet();
        return new NuitonValidatorModel<>(new NuitonValidatorModelEntry<>(type, keyContext, scopes), fields);
    }
}

