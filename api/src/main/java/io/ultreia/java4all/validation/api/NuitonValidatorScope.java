/*
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.validation.api;

import io.ultreia.java4all.i18n.spi.enumeration.TranslateEnumeration;

/**
 * The different levels of messages in validation process.
 * <p>
 * The order of the enum defines the severity of validation.
 * <p>
 * Always begin with fatal, then error, then if no error found, try warning, then info...
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
@TranslateEnumeration(name = "label", pattern = "validation.scope.@NAME@")
public enum NuitonValidatorScope {

    /**
     * the fatal error scope level.
     * <p>
     * When a message of a such scope is found on a validator, then the
     * validator is invalid and modified.
     */
    FATAL,
    /**
     * the error scope level.
     * <p>
     * When a message of a such scope is found on a validator, then the
     * validator is invalid and modified.
     */
    ERROR,
    /**
     * the warning scope level.
     * <p>
     * When a message of a such scope is found on a validator, then the
     * validator is still valid but modified.
     */
    WARNING,
    /**
     * the information scope level.
     * <p>
     * When a message of a sucg scope is found on a validator, then the
     * validator is still valid and not modified.
     */
    INFO;

    public String getLabel() {
        return NuitonValidatorScopeI18n.getLabel(this);
    }
}
