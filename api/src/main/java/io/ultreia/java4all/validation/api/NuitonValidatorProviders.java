package io.ultreia.java4all.validation.api;

/*-
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.TreeMap;

/**
 * To get a {@link NuitonValidatorProvider}.
 *
 * <p>
 * Created at 05/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonValidatorProviders {
    private static final Logger log = LogManager.getLogger(NuitonValidatorProviders.class);

    protected static String defaultFactoryName;

    protected static Map<String, NuitonValidatorProviderFactory> factories;

    protected NuitonValidatorProviders() {
        // avoid instantiation of this factory
    }

    public static NuitonValidatorProvider newProvider(String providerName, NuitonValidationContext validationContext) {
        if (providerName == null) {
            // take the default validator provider name
            throw new NullPointerException("providerName parameter can not be null.");
        }
        return getFactory(providerName).newProvider(validationContext);
    }

    public static String getDefaultFactoryName() {
        if (defaultFactoryName == null) {
            Map<String, NuitonValidatorProviderFactory> factories = getFactories();
            if (factories.isEmpty()) {
                throw new IllegalStateException("Could not find any provider factory.");
            }
            Collection<NuitonValidatorProviderFactory> candidates = factories.values();

            if (candidates.size() > 1) {
                throw new IllegalStateException(String.format("There is %d provider factories, can't choose for you. You MUST invoke method io.ultreia.java4all.validation.api.NuitonValidatorProviders.setDefaultFactoryName(\"XXX\") where \"XXX\" is from: %s", candidates.size(), factories.keySet()));
            }
            defaultFactoryName = candidates.iterator().next().getName();
            log.info("Set the default provider name to {}", defaultFactoryName);
        }
        return defaultFactoryName;
    }

    public static void setDefaultFactoryName(String defaultFactoryName) {
        // check factory exists
        getFactory(defaultFactoryName);
        NuitonValidatorProviders.defaultFactoryName = Objects.requireNonNull(defaultFactoryName);
        log.info("Set the default provider name to {}", defaultFactoryName);
    }

    private static Map<String, NuitonValidatorProviderFactory> getFactories() {
        if (factories == null) {
            factories = new TreeMap<>();
            ServiceLoader<NuitonValidatorProviderFactory> serviceLoader = ServiceLoader.load(NuitonValidatorProviderFactory.class);
            for (NuitonValidatorProviderFactory factory : serviceLoader) {
                log.info("obtain validator provider factory {}", factory.getName());
                factories.put(factory.getName(), factory);
            }
        }
        return factories;
    }

    private static NuitonValidatorProviderFactory getFactory(String providerName) {
        NuitonValidatorProviderFactory factory = getFactories().get(providerName);
        if (factory == null) {
            throw new IllegalArgumentException(String.format("Could not find provider factory named '%s', existing providers are : %s", providerName, getFactories().keySet()));
        }
        return factory;
    }
}
