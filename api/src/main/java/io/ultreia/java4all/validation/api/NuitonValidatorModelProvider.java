package io.ultreia.java4all.validation.api;

/*-
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.io.NuitonValidatorModelHelper;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * Created at 05/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonValidatorModelProvider {

    private final String name;
    protected Set<NuitonValidatorModel<?>> models;

    public NuitonValidatorModelProvider(String name) {
        this.name = name;
    }

    /**
     * Obtain a validator model, the model should be cached and not be instantiated at each time a validator model is asked.
     *
     * @param type    type of the class to validate
     * @param context context of validation ({@code null} if no context)
     * @param scopes  filtered scope (if nothing given, then use all scopes)
     * @param <O>     type of the class to validate
     * @return the cached model of validation
     */
    public <O> NuitonValidatorModel<O> getModel(Class<O> type, String context, NuitonValidatorScope... scopes) {
        NuitonValidatorModelEntry<O> key = NuitonValidatorModelEntry.of(type, context, scopes);
        @SuppressWarnings({"unchecked"})
        NuitonValidatorModel<O> model = (NuitonValidatorModel<O>) getModels().stream().filter(m -> m.getKey().matchTypeAndContext(key)).findFirst().orElse(null);
        if (model == null) {
            // creates an empty model and store it in cache
            model = new NuitonValidatorModel<>(key, Map.of());
            models.add(model);
        }
        return Objects.requireNonNull(model, String.format("Could not find model for entry: %s", key)).toModel(key);
    }

    //FIXME Remove this when removing legacy code
    public <O> Optional<NuitonValidatorModel<O>> getOptionalModel(Class<O> type, String context, NuitonValidatorScope... scopes) {
        NuitonValidatorModelEntry<O> key = NuitonValidatorModelEntry.of(type, context, scopes);
        @SuppressWarnings({"unchecked"})
        NuitonValidatorModel<O> model = (NuitonValidatorModel<O>) getModels().stream().filter(m -> m.getKey().matchTypeAndContext(key)).findFirst().orElse(null);
        if (model == null) {
            return Optional.empty();
        }
        return Optional.of(model.toModel(key));
    }

    protected Set<NuitonValidatorModel<?>> getModels() {
        if (models == null) {
            try {
                List<NuitonValidatorModel<?>> validatorModels = new NuitonValidatorModelHelper().readAll(name);
                models = new LinkedHashSet<>(validatorModels);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return models;
    }

}
