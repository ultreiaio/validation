package io.ultreia.java4all.validation.api;

/*-
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 26/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public abstract class AbstractNuitonValidator<O> implements NuitonValidator<O> {

    /**
     * Bean validation model.
     */
    private final NuitonValidatorModel<O> model;

    /**
     * Bean scope validators (lazy loaded).
     */
    private Map<NuitonValidatorScope, NuitonScopeValidator<O>> validators;


    protected AbstractNuitonValidator(NuitonValidatorModel<O> model) {
        this.model = Objects.requireNonNull(model);
    }

    protected abstract NuitonScopeValidator<O> createScopeValidator(String context, NuitonValidatorScope scope, Class<O> type, Set<String> fields);

    @Override
    public NuitonValidatorModel<O> getModel() {
        return model;
    }

    @Override
    public NuitonValidatorResult validate(O object, NuitonValidationContext validationContext) throws NullPointerException {

        Objects.requireNonNull(object);

        NuitonValidatorResult result = new NuitonValidatorResult();

        for (Map.Entry<NuitonValidatorScope, NuitonScopeValidator<O>> entry : getValidators().entrySet()) {
            NuitonValidatorScope scope = entry.getKey();
            NuitonScopeValidator<O> validator = entry.getValue();
            Map<String, List<String>> newMessages = validator.validate(object, validationContext);
            result.addMessagesForScope(scope, newMessages);
        }
        return result;
    }

    @Override
    public Set<NuitonValidatorScope> getEffectiveScopes() {
        return getValidators().keySet();
    }

    @Override
    public Set<String> getEffectiveFields() {
        return getValidators().values().stream().flatMap(s -> s.getFieldNames().stream()).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getEffectiveFields(NuitonValidatorScope scope) {
        Set<String> result = new HashSet<>();
        NuitonScopeValidator<O> scopeValidator = getValidators().get(scope);
        if (scopeValidator != null) {
            result.addAll(scopeValidator.getFieldNames());
        }
        return result;
    }

    protected Map<NuitonValidatorScope, NuitonScopeValidator<O>> getValidators() {
        if (validators == null) {
            Map<NuitonValidatorScope, NuitonScopeValidator<O>> validators = new EnumMap<>(NuitonValidatorScope.class);
            Class<O> type = model.getType();
            String context = model.getContext();
            for (NuitonValidatorScope scope : model.getScopes()) {
                model.getFieldNames(scope).ifPresent(fieldNames -> {
                    NuitonScopeValidator<O> newValidator = createScopeValidator(context, scope, type, fieldNames);
                    validators.put(scope, newValidator);
                });
            }
            this.validators = Collections.unmodifiableMap(validators);
        }
        return validators;
    }

}
