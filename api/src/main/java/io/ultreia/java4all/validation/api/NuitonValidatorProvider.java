/*
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.validation.api;

import java.nio.file.Path;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * Provider of {@link NuitonValidator}.
 * <p>
 * An implementation of a such class provides a implementation of a validator models
 * and also of validator.
 * <p>
 * <b>Note:</b> Providers are used in the {@link NuitonValidatorProviders} and
 * should be registered via the {@link ServiceLoader} api.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see NuitonValidatorModel
 * @see NuitonValidator
 * @see ServiceLoader
 * @since 2.0
 */
public interface NuitonValidatorProvider {

    /**
     * Obtains the name of the provider.
     *
     * @return the name of the provider.
     */
    String getName();

    NuitonValidatorModelProvider getModelProvider();

    NuitonValidationContext getValidationContext();

    /**
     * Obtains a new validator for the given {@code model}.
     *
     * @param model the model of validator to use
     * @param <O>   type of class to validate
     * @return the new validator
     */
    <O> NuitonValidator<O> newValidator(NuitonValidatorModel<O> model);

    /**
     * @param entry    validator file entry
     * @param rootPath the root path where to find validation files
     * @param scope    the required scope
     * @return the path to the validation file
     */
    Path toPath(NuitonValidatorModelEntry<?> entry, Path rootPath, NuitonValidatorScope scope);

    /**
     * @param file    validation file
     * @param type    type of data
     * @param context context of validation
     * @param scope   scope of validation
     * @param fields  fields used for this validation
     * @return the corresponding validation file info
     */
    NuitonValidatorFileInfo toFileInfo(Path file, Class<?> type, String context, NuitonValidatorScope scope, Set<String> fields);

    /**
     * Obtains a new validator for the given {@code model}.
     *
     * @param type    type of the class to validate
     * @param context context of validation ({@code null} if no context)
     * @param scopes  filtered scope (if nothing given, then use all scopes)
     * @param <O>     type of class to validate
     * @return the new validator
     */
    default <O> NuitonValidator<O> newValidator(Class<O> type, String context, NuitonValidatorScope... scopes) {
        if (type == null) {
            throw new NullPointerException("type parameter can not be null.");
        }
        // obtain validator model form the provider
        NuitonValidatorModel<O> model = getModelProvider().getModel(type, context, scopes);
        // obtain validator from the the provider
        return newValidator(model);
    }
}
