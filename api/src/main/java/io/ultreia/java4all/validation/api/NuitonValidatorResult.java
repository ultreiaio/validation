/*
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.validation.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Contains validation messages coming from the method
 * {@link NuitonValidator#validate(Object, NuitonValidationContext)}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class NuitonValidatorResult {

    protected EnumMap<NuitonValidatorScope, FieldMap<List<String>>> messages;
    protected Map<String, FieldMap<Object>> tagValues;

    public static class FieldMap<V> extends TreeMap<String, V> {

        private static final long serialVersionUID = 1L;
    }

    public boolean isValid() {
        return !hasFatalMessages() && !hasErrorMessagess();
    }

    public void clear() {
        messages.clear();
    }

    public boolean isEmpty() {
        return messages.isEmpty();
    }

    public boolean hasMessagesForScope(NuitonValidatorScope scope) {
        boolean result = false;
        if (messages != null) {
            FieldMap<List<String>> map = messages.get(scope);
            result = map != null && !map.isEmpty();
        }
        return result;
    }

    public boolean hasMessagesForScope(String field,
                                       NuitonValidatorScope scope) {
        boolean result = false;
        if (messages != null) {
            result = messages.containsKey(scope);

            if (result) {
                FieldMap<List<String>> fieldMap = messages.get(scope);
                result = fieldMap != null && fieldMap.containsKey(field);
            }
        }
        return result;
    }

    public boolean hasFatalMessages() {
        boolean result = hasMessagesForScope(NuitonValidatorScope.FATAL);
        return result;
    }

    public boolean hasErrorMessagess() {
        boolean result = hasMessagesForScope(NuitonValidatorScope.ERROR);
        return result;
    }

    public boolean hasInfoMessages() {
        boolean result = hasMessagesForScope(NuitonValidatorScope.INFO);
        return result;
    }

    public boolean hasWarningMessages() {
        boolean result = hasMessagesForScope(NuitonValidatorScope.WARNING);
        return result;
    }

    public void addMessagesForScope(NuitonValidatorScope scope,
                                    Map<String, List<String>> newMessages) {
        if (messages == null) {
            messages = new EnumMap<>(NuitonValidatorScope.class);
        }

        FieldMap<List<String>> fieldMap = messages.get(scope);

        if (fieldMap == null) {
            fieldMap = new FieldMap<>();
            messages.put(scope, fieldMap);
        }

        for (Map.Entry<String, List<String>> entry : newMessages.entrySet()) {
            String fieldName = entry.getKey();
            List<String> messages = entry.getValue();
            List<String> oldMessages = fieldMap.get(fieldName);
            if (oldMessages == null) {
                oldMessages = messages;
                fieldMap.put(fieldName, oldMessages);
            } else {
                oldMessages.addAll(messages);
            }
        }
    }

    public void setMessagesForScope(NuitonValidatorScope scope,
                                    String field,
                                    List<String> messages) {

        if (this.messages == null) {
            this.messages = new EnumMap<>(NuitonValidatorScope.class);
        }

        FieldMap<List<String>> fieldMap = this.messages.get(scope);
        if (fieldMap == null) {
            fieldMap = new FieldMap<>();
            this.messages.put(scope, fieldMap);
        }
        fieldMap.put(field, messages);
    }

    public List<String> getMessagesForScope(NuitonValidatorScope scope) {

        List<String> result = new ArrayList<>();
        if (messages != null) {
            FieldMap<List<String>> fieldMap = messages.get(scope);
            for (List<String> messages : fieldMap.values()) {
                result.addAll(messages);
            }
        }
        return result;
    }

    public List<String> getMessagesForScope(String field,
                                            NuitonValidatorScope scope) {

        List<String> result = null;
        if (messages != null) {
            FieldMap<List<String>> fieldMap = messages.get(scope);
            result = fieldMap.get(field);
        }
        if (result == null) {
            result = Collections.emptyList();
        }
        return result;
    }

    public List<String> getFatalMessages(String field) {
        List<String> result =
                getMessagesForScope(field, NuitonValidatorScope.FATAL);
        return result;
    }

    public List<String> getErrorMessages(String field) {
        List<String> result =
                getMessagesForScope(field, NuitonValidatorScope.ERROR);
        return result;
    }

    public List<String> getInfoMessages(String field) {
        List<String> result =
                getMessagesForScope(field, NuitonValidatorScope.INFO);
        return result;
    }

    public List<String> getWarningMessages(String field) {
        List<String> result =
                getMessagesForScope(field, NuitonValidatorScope.WARNING);
        return result;
    }

    public Map<String, Object> getTagValues(String field) {
        Map<String, Object> result = null;
        if (tagValues != null) {
            result = tagValues.get(field);
        }
        if (result == null) {
            result = Collections.emptyMap();
        }
        return result;
    }

    public List<String> getFieldsForScope(NuitonValidatorScope scope) {

        List<String> result = null;
        if (messages != null) {
            FieldMap<List<String>> fieldMap = messages.get(scope);
            if (fieldMap != null) {
                result = new ArrayList<>(fieldMap.keySet());
            }
        }
        if (result == null) {
            result = Collections.emptyList();
        }
        return result;
    }

    public List<String> getFieldsForFatal() {
        List<String> result = getFieldsForScope(NuitonValidatorScope.FATAL);
        return result;
    }

    public List<String> getFieldsForError() {
        List<String> result = getFieldsForScope(NuitonValidatorScope.ERROR);
        return result;
    }

    public List<String> getFieldsForInfo() {
        List<String> result = getFieldsForScope(NuitonValidatorScope.INFO);
        return result;
    }

    public List<String> getFieldsForWarning() {
        List<String> result = getFieldsForScope(NuitonValidatorScope.WARNING);
        return result;
    }

    public void clearMessagesForScope(NuitonValidatorScope scope) {
        if (messages != null) {
            messages.remove(scope);
        }
    }

    public NuitonValidatorScope getFieldHighestScope(String field) {
        if (messages == null) {
            return null;
        }
        if (containsField(field, NuitonValidatorScope.FATAL)) {
            return NuitonValidatorScope.FATAL;
        }
        if (containsField(field, NuitonValidatorScope.ERROR)) {
            return NuitonValidatorScope.ERROR;
        }
        if (containsField(field, NuitonValidatorScope.WARNING)) {
            return NuitonValidatorScope.WARNING;
        }
        if (containsField(field, NuitonValidatorScope.INFO)) {
            return NuitonValidatorScope.INFO;
        }

        // no scope for the field
        return null;
    }

    public NuitonValidatorScope[] getFieldScopes(String field) {
        Set<NuitonValidatorScope> result = new HashSet<>();
        if (messages != null) {

            if (containsField(field, NuitonValidatorScope.FATAL)) {
                result.add(NuitonValidatorScope.FATAL);
            }
            if (containsField(field, NuitonValidatorScope.ERROR)) {
                result.add(NuitonValidatorScope.ERROR);
            }
            if (containsField(field, NuitonValidatorScope.WARNING)) {
                result.add(NuitonValidatorScope.WARNING);
            }
            if (containsField(field, NuitonValidatorScope.INFO)) {
                result.add(NuitonValidatorScope.INFO);
            }
        }

        return result.toArray(new NuitonValidatorScope[result.size()]);
    }

    protected boolean containsField(String field, NuitonValidatorScope scope) {
        FieldMap<List<String>> fieldMap = messages.get(scope);
        return fieldMap != null && fieldMap.containsKey(field);
    }

    protected EnumMap<NuitonValidatorScope, FieldMap<List<String>>> getMessages() {
        return messages;
    }

    protected Map<String, FieldMap<Object>> getTagValues() {
        return tagValues;
    }
}
