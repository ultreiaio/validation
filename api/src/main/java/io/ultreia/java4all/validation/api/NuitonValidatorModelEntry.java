package io.ultreia.java4all.validation.api;

/*-
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;

/**
 * Created at 06/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonValidatorModelEntry<O> {

    /**
     * Type of object to validate
     */
    protected final Class<O> type;
    /**
     * Context of validation (can be {@code null}, for no context).
     */
    protected final String context;
    /**
     * Set of scopes that can be validated for the type and context
     */
    protected final Set<NuitonValidatorScope> scopes;

    public NuitonValidatorModelEntry(Class<O> type, String context, Set<NuitonValidatorScope> scopes) {
        this.type = type;
        this.context = context;
        this.scopes = scopes;
    }

    public static <O> NuitonValidatorModelEntry<O> of(Class<O> type, String context, NuitonValidatorScope... scopes) {
        return new NuitonValidatorModelEntry<>(type, context, new LinkedHashSet<>(List.of(scopes.length == 0 ? NuitonValidatorScope.values() : scopes)));
    }

    public Class<O> getType() {
        return type;
    }

    public String getContext() {
        return context;
    }

    public Set<NuitonValidatorScope> getScopes() {
        return scopes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NuitonValidatorModelEntry)) return false;
        NuitonValidatorModelEntry<?> that = (NuitonValidatorModelEntry<?>) o;
        return Objects.equals(type, that.type) && Objects.equals(context, that.context) && Objects.equals(scopes, that.scopes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, context, scopes);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", NuitonValidatorModelEntry.class.getSimpleName() + "[", "]")
                .add("type=" + type)
                .add("context='" + context + "'")
                .add("scopes=" + scopes)
                .toString();
    }

    public <Y> boolean matchTypeAndContext(NuitonValidatorModelEntry<Y> that) {
        if (!Objects.equals(type, that.type) || !Objects.equals(context, that.context)) {
            return false;
        }
        for (NuitonValidatorScope scope : that.scopes) {
            if (scopes.contains(scope)) {
                return true;
            }
        }
        //FIXME Find out what to put
        return false;
    }

    public Path toPath(Path rootPath, NuitonValidatorScope scope) {
        return rootPath.resolve(type.getPackageName().replaceAll("\\.", "/")).resolve(String.format("%s%s-%s-validation.xml", type.getSimpleName(), (context == null ? "" : "-" + context), scope.name().toLowerCase()));
    }
}
