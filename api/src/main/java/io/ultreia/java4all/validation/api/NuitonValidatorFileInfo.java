package io.ultreia.java4all.validation.api;

/*-
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;

/**
 * Created at 09/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public abstract class NuitonValidatorFileInfo {

    private transient final Path file;
    private final Class<?> type;
    private final String context;
    private final NuitonValidatorScope scope;
    private final Set<String> fields;
    private Map<String, List<String>> comments;

    public NuitonValidatorFileInfo(Path file, Class<?> type, String context, NuitonValidatorScope scope, Set<String> fields) {
        this.file = file;
        this.type = type;
        this.context = context;
        this.scope = scope;
        this.fields = fields;
    }

    public Path getFile() {
        return file;
    }

    public String getOrderKey() {
        return type.getName() + "-" + context + "-" + scope;
    }

    public Class<?> getType() {
        return type;
    }

    public String getContext() {
        return context;
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    public Set<String> getFields() {
        return fields;
    }

    protected abstract Map<String, List<String>> createComments();

    public final Map<String, List<String>> getComments() {
        if (comments == null) {
            comments = createComments();
        }
        return comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NuitonValidatorFileInfo that = (NuitonValidatorFileInfo) o;
        return Objects.equals(type, that.type) && Objects.equals(context, that.context) && scope == that.scope;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, context, scope);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", NuitonValidatorFileInfo.class.getSimpleName() + "[", "]")
                .add("type=" + type.getName())
                .add("context='" + context + "'")
                .add("scope=" + scope)
                .add("fields=" + getFields())
                .toString();
    }
}
