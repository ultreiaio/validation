package io.ultreia.java4all.validation.api.io;

/*-
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.util.json.adapters.ClassAdapter;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created at 06/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonValidatorModelHelper {
    public static final String LOCATION = "META-INF/validation/model-%s.json";
    private static final Logger log = LogManager.getLogger(NuitonValidatorModelHelper.class);

    public static Gson creatGson() {
        return new GsonBuilder().setPrettyPrinting()
                                .registerTypeAdapter(Class.class, new ClassAdapter())
                                .registerTypeAdapter(NuitonValidatorModel.class, new NuitonValidatorModelAdapter<>())
                                .disableHtmlEscaping()
                                .create();
    }

    public List<NuitonValidatorModel<?>> readAll(String name) throws IOException {
        Enumeration<URL> resource = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResources(toLocation(name)));
        Gson gson = creatGson();
        List<NuitonValidatorModel<?>> result = new LinkedList<>();
        while (resource.hasMoreElements()) {
            URL url = resource.nextElement();
            result.addAll(readFile(gson, url));
        }
        return result.stream().sorted(Comparator.comparing(m -> m.getKey().toString(), String::compareTo)).collect(Collectors.toList());
    }

    public List<NuitonValidatorModel<?>> read(String name, Path location) throws IOException {
        URL resource = Objects.requireNonNull(location.resolve(toLocation(name)).toUri().toURL());
        Gson gson = creatGson();
        return readFile(gson, resource).stream().sorted(Comparator.comparing(m -> m.getKey().toString(), String::compareTo)).collect(Collectors.toList());
    }

    public Path write(String name, List<NuitonValidatorModel<?>> models, Path target) throws IOException {
        Gson gson = creatGson();
        String content = gson.toJson(models);
        Path location = toLocation(name, target);
        if (Files.notExists(location.getParent())) {
            Files.createDirectories(location.getParent());
        }
        Files.writeString(location, content);
        return location;
    }


    public Path toLocation(String name, Path target) {
        return target.resolve(toLocation(name));
    }

    public String toLocation(String name) {
        return String.format(LOCATION, name);
    }

    private List<NuitonValidatorModel<?>> readFile(Gson gson, URL url) throws IOException {
        log.info("Loading file: {}", url);
        try (Reader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            return gson.fromJson(reader, new TypeToken<List<NuitonValidatorModel<?>>>() {
            }.getType());
        }
    }
}


