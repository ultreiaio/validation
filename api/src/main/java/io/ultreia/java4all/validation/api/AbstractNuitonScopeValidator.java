package io.ultreia.java4all.validation.api;

/*-
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Set;

/**
 * Created on 26/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public abstract class AbstractNuitonScopeValidator<O> implements NuitonScopeValidator<O> {
    /**
     * the type of bean to validate
     */
    protected final Class<O> type;

    /**
     * the validation named context (can be null)
     */
    private final String context;

    /**
     * the list of field names detected for this validator
     */
    private final Set<String> fieldNames;

    protected AbstractNuitonScopeValidator(Class<O> type, String context, Set<String> fieldNames) {
        this.type = type;
        this.context = context;
        this.fieldNames = fieldNames;
    }

    @Override
    public Class<O> getType() {
        return type;
    }

    @Override
    public String getContext() {
        return context;
    }

    @Override
    public Set<String> getFieldNames() {
        return fieldNames;
    }

    @Override
    public boolean containsField(String fieldName) {
        return fieldNames.contains(fieldName);
    }


    @Override
    public String toString() {
        return String.format("%s<beanClass:%s, contextName:%s>", super.toString(), type, context);
    }


}
