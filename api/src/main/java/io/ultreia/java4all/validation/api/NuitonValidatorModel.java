/*
 * #%L
 * Validation :: API
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.validation.api;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Represents the model of a {@link NuitonValidator}.
 * <p>
 * This model describing properties of a validator :
 * <ul>
 * <li>{@link #key} : TODO</li>
 * <li>{@link #fieldsByScope} : the field models per scope</li>
 * </ul>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class NuitonValidatorModel<O> {

    protected final NuitonValidatorModelEntry<O> key;
    /**
     * Field models by scope.
     */
    protected final Map<NuitonValidatorScope, List<NuitonFieldValidatorModel>> fieldsByScope;

    public NuitonValidatorModel(NuitonValidatorModelEntry<O> key, Map<NuitonValidatorScope, List<NuitonFieldValidatorModel>> fieldsByScope) {
        this.key = Objects.requireNonNull(key);
        this.fieldsByScope = Collections.unmodifiableMap(Objects.requireNonNull(fieldsByScope));
    }

    public NuitonValidatorModelEntry<O> getKey() {
        return key;
    }

    public Class<O> getType() {
        return key.getType();
    }

    public String getContext() {
        return key.getContext();
    }

    public Set<NuitonValidatorScope> getScopes() {
        return key.getScopes();
    }

    public Map<NuitonValidatorScope, List<NuitonFieldValidatorModel>> getFieldsByScope() {
        return fieldsByScope;
    }

    public Optional<Set<String>> getFieldNames(NuitonValidatorScope scope) {
        return Optional.ofNullable(getFieldsByScope().get(scope)).map(e->e.stream().map(NuitonFieldValidatorModel::getFieldName).collect(Collectors.toCollection(LinkedHashSet::new)));
    }
    @Override
    public String toString() {
        return new StringJoiner(", ", NuitonValidatorModel.class.getSimpleName() + "[", "]")
                .add("key=" + key)
                .add("fieldsByScope=" + fieldsByScope)
                .toString();
    }

    public NuitonValidatorModel<O> toModel(NuitonValidatorModelEntry<O> key) {
        if (key.equals(getKey())) {
            return this;
        }
        Map<NuitonValidatorScope, List<NuitonFieldValidatorModel>> newFieldsByScope = new TreeMap<>();
        for (NuitonValidatorScope scope : key.getScopes()) {
            List<NuitonFieldValidatorModel> value = fieldsByScope.get(scope);
            if (value != null) {
                newFieldsByScope.put(scope, value);
            }
        }
        return new NuitonValidatorModel<>(key, newFieldsByScope);
    }
}
