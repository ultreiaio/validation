# Java4all Validation changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2024-09-19 11:49.

## Version [2.0.0-RC-7](https://gitlab.com/ultreiaio/validation/-/milestones/11)

**Closed at 2024-09-19.**


### Issues
  * [[enhancement 18]](https://gitlab.com/ultreiaio/validation/-/issues/18) **Improve where to generate the mapping.json file (in GenerateValidatorsMojo mojo)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.0-RC-6](https://gitlab.com/ultreiaio/validation/-/milestones/10)

**Closed at 2024-09-18.**


### Issues
  * [[enhancement 17]](https://gitlab.com/ultreiaio/validation/-/issues/17) **At last, remove xworks technology** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.0-RC-5](https://gitlab.com/ultreiaio/validation/-/milestones/9)

**Closed at 2024-09-13.**


### Issues
  * [[bug 16]](https://gitlab.com/ultreiaio/validation/-/issues/16) **Fix DateSupport validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.0-RC-4](https://gitlab.com/ultreiaio/validation/-/milestones/8)

**Closed at 2024-09-10.**


### Issues
  * [[enhancement 15]](https://gitlab.com/ultreiaio/validation/-/issues/15) **NuitonValidatorModelProvider.getModel should always return a not null model (if not found in cache, create it)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.0-RC-3](https://gitlab.com/ultreiaio/validation/-/milestones/7)

**Closed at 2024-02-22.**


### Issues
No issue.

## Version [2.0.0-RC-2](https://gitlab.com/ultreiaio/validation/-/milestones/6)

**Closed at 2024-02-13.**


### Issues
  * [[enhancement 14]](https://gitlab.com/ultreiaio/validation/-/issues/14) **Rethink the project** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [2.0.0-RC-1](https://gitlab.com/ultreiaio/validation/-/milestones/5)
Introduce a new implementation of NuitonValidator which does not used XWorks at all&#13;&#10;

**Closed at 2024-02-05.**


### Issues
  * [[enhancement 8]](https://gitlab.com/ultreiaio/validation/-/issues/8) **Split module API into two modules API (which only contains API) and xworks** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 9]](https://gitlab.com/ultreiaio/validation/-/issues/9) **Remove the BeanListValidator API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 10]](https://gitlab.com/ultreiaio/validation/-/issues/10) **Improve API before implementing a new one** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 11]](https://gitlab.com/ultreiaio/validation/-/issues/11) **Replace SimpleBeanValidator API by BeanValidator API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 12]](https://gitlab.com/ultreiaio/validation/-/issues/12) **Introduce a new SPI module for next generation validator API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 13]](https://gitlab.com/ultreiaio/validation/-/issues/13) **Introduce a new Mojo module to manage next generation API using the SPI module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.3](https://gitlab.com/ultreiaio/validation/-/milestones/4)

**Closed at 2022-05-03.**


### Issues
  * [[bug 6]](https://gitlab.com/ultreiaio/validation/-/issues/6) **Text provider not set in validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 7]](https://gitlab.com/ultreiaio/validation/-/issues/7) **Remove usage of old libraries...** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/ultreiaio/validation/-/milestones/3)

**Closed at 2022-03-10.**


### Issues
  * [[bug 4]](https://gitlab.com/ultreiaio/validation/-/issues/4) **Fix index in CollectionFieldExpressionValidator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 5]](https://gitlab.com/ultreiaio/validation/-/issues/5) **Fix some minor API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.1](https://gitlab.com/ultreiaio/validation/-/milestones/2)

**Closed at 2021-09-06.**


### Issues
  * [[enhancement 2]](https://gitlab.com/ultreiaio/validation/-/issues/2) **Add i18n** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 3]](https://gitlab.com/ultreiaio/validation/-/issues/3) **Clean code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.0](https://gitlab.com/ultreiaio/validation/-/milestones/1)

**Closed at 2021-01-31.**


### Issues
  * [[support 1]](https://gitlab.com/ultreiaio/validation/-/issues/1) **Initial import from nuiton-validator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

