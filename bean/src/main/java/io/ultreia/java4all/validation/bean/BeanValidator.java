/*
 * #%L
 * Validation :: Bean
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.validation.bean;

import io.ultreia.java4all.validation.api.NuitonValidator;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorResult;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.event.EventListenerList;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Validator for a javaBean object.
 * <p>
 * A such validator is designed to validate to keep the validation of a bean,
 * means the bean is attached to the validator (via the context field {@link #context}.
 * <p>
 * A such validator is also a JavaBean and you can listen his states
 * modifications via the classic java bean api.
 * <p>
 * <strong>Note:</strong> The {@link BeanValidator} should never be used for
 * validation in a service approach since it needs to keep a reference to the
 * bean to validate.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see BeanValidatorListener
 * @since 2.5.2
 */
public class BeanValidator<O> {

    /**
     * Name of the bounded property {@code bean}.
     *
     * @see #getBean()
     * @see #setBean(Object)
     */
    public static final String BEAN_PROPERTY = "bean";
    /**
     * Name of the bounded property {@code context}.
     *
     * @see #getContext()
     * @see #setContext(String)
     */
    public static final String CONTEXT_PROPERTY = "context";
    /**
     * Name of the bounded property {@code scopes}.
     *
     * @see #getScopes()
     * @see #setScopes(NuitonValidatorScope...)
     */
    public static final String SCOPES_PROPERTY = "scopes";
    /**
     * Name of the bounded property {@link #valid}.
     *
     * @see #valid
     * @see #isValid()
     * @see #setValid(boolean)
     */
    public static final String VALID_PROPERTY = "valid";
    /**
     * Name of the bounded property {@link #changed}.
     *
     * @see #changed
     * @see #isChanged()
     * @see #setChanged(boolean)
     */
    public static final String CHANGED_PROPERTY = "changed";

    private static final Logger log = LogManager.getLogger(BeanValidator.class);
    /**
     * Listener that listens on bean modification.
     */
    protected final PropertyChangeListener l;
    /**
     * delegate property change support
     */
    protected final PropertyChangeSupport pcs;
    /**
     * A list of event listeners for this validators
     */
    protected final EventListenerList listenerList = new EventListenerList();
    /**
     * The provider of delegate validators.
     * <p>
     * It will also produce validator model.
     *
     * @see NuitonValidatorProvider
     */
    protected final NuitonValidatorProvider validatorProvider;
    /**
     * Context of the registered bean to validate.
     *
     * @since 2.5.2
     */
    protected final BeanValidatorContext<O> context;
    /**
     * State to indicate that validator has changed since the last time bean was set.
     */
    protected boolean changed;
    /**
     * State of the validator (is true if no errors of error scope is found).
     */
    protected boolean valid = true;
    /**
     * State to know if the validator can be used (we keep this state for
     * performance reasons : do not want to compute this value each time a
     * validation is asked...).
     */
    protected boolean canValidate = true;
    /**
     * To chain to another validator (acting as parent of this one).
     *
     * @since 2.5.2
     */
    protected BeanValidator<?> parentValidator;

    public BeanValidator(NuitonValidatorProvider validatorProvider, Class<O> beanClass, String context, NuitonValidatorScope... scopes) {

        // check if given bean class is Javabean compliant
        boolean javaBeanCompliant = BeanUtil.isJavaBeanCompliant(beanClass);

        if (!javaBeanCompliant) {
            throw new IllegalStateException(String.format("%s is not JavaBean compliant (%s, or %s method not found).", beanClass.getName(), BeanUtil.ADD_PROPERTY_CHANGE_LISTENER, BeanUtil.REMOVE_PROPERTY_CHANGE_LISTENER));
        }
        this.validatorProvider = validatorProvider;

        pcs = new PropertyChangeSupport(this);

        l = evt -> {
            @SuppressWarnings("unchecked") O bean = (O) evt.getSource();
            // the bean has changed, replay validation
            doValidate(bean);
        };

        this.context = new BeanValidatorContext<>(validatorProvider.getValidationContext());

        // build delegate validator
        rebuildDelegateValidator(beanClass, context, scopes);

        // context has changed
        firePropertyChange(CONTEXT_PROPERTY, null, context);

        // scopes has changed
        firePropertyChange(SCOPES_PROPERTY, null, scopes);
    }

    /**
     * Obtain the actual bean attached to the validator.
     *
     * @return the bean attached to the validator or {@code null} if no bean
     * is attached
     */
    public O getBean() {
        return context.getBean();
    }

    /**
     * Change the attached bean.
     * <p>
     * As a side effect, the internal
     * {@link BeanValidatorContext#getMessages()} will be reset.
     *
     * @param bean the bean to attach (can be {@code null} to reset the
     *             validator).
     */
    public void setBean(O bean) {
        O oldBean = getBean();
        if (log.isDebugEnabled()) {
            log.debug(this + " : " + bean);
        }

        if (oldBean != null) {
            try {
                BeanUtil.removePropertyChangeListener(l, oldBean);
            } catch (Exception eee) {
                if (log.isInfoEnabled()) {
                    log.info(String.format("Can't unregister as listener for bean %s for reason %s", oldBean.getClass(), eee.getMessage()), eee);
                }
            }
        }
        context.setBean(bean);

        if (bean == null) {

            // remove all messages for all fields of the validator

            mergeMessages(null);

        } else {
            try {

                BeanUtil.addPropertyChangeListener(l, bean);
            } catch (Exception eee) {
                if (log.isInfoEnabled()) {
                    log.info(String.format("Can't register as listener for bean %s for reason %s", bean.getClass(), eee.getMessage()), eee);
                }
            }
            validate();
        }
        setChanged(false);
        setValid(context.isValid());
        firePropertyChange(BEAN_PROPERTY, oldBean, bean);
    }

    public BeanValidator<?> getParentValidator() {
        return parentValidator;
    }

    public void setParentValidator(BeanValidator<?> parentValidator) {
        this.parentValidator = parentValidator;
    }

    public boolean hasFatalErrors() {
        return context.hasFatalErrors();
    }

    public boolean hasErrors() {
        return context.hasErrors();
    }

    public boolean hasWarnings() {
        return context.hasWarnings();
    }

    public boolean hasInfos() {
        return context.hasInfos();
    }

    public boolean isValid(String fieldName) {
        // field is valid if no fatal messages nor error messages
        return context.isValid(fieldName);
    }

    public NuitonValidatorScope getHighestScope(String field) {
        return context.getHighestScope(field);
    }

    public void doValidate() {
        validate();
        setValid(context.isValid());
        setChanged(true);
    }

    public void addBeanValidatorListener(BeanValidatorListener listener) {
        listenerList.add(BeanValidatorListener.class, listener);
    }

    public void removeBeanValidatorListener(BeanValidatorListener listener) {
        listenerList.remove(BeanValidatorListener.class, listener);
    }

    public BeanValidatorListener[] getBeanValidatorListeners() {
        return listenerList.getListeners(BeanValidatorListener.class);
    }


    /**
     * Obtain the {@link #changed} property value.
     * <p>
     * Returns {@code true} if bean was modified since last time a bean was attached.
     *
     * @return {@code true} if bean was modified since last attachment of
     * a bean.
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * To force the value of the property {@link #changed}.
     *
     * @param changed flag to force reset of property {@link #changed}
     */
    public void setChanged(boolean changed) {
        this.changed = changed;

        // force the property to be fired (never pass the older value)
        firePropertyChange(BeanValidator.CHANGED_PROPERTY, null, changed);
    }

    public boolean isCanValidate() {
        return canValidate;
    }

    public void setCanValidate(boolean canValidate) {
        this.canValidate = canValidate;
    }

    /**
     * Obtain the {@link #valid} property value.
     *
     * @return {@code true} if attached bean is valid (no error or fatal messages)
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * Change the value of the {@link #valid} property.
     *
     * @param valid the new value of the property
     */
    public void setValid(boolean valid) {
        this.valid = valid;

        // force the property to be fired (never pass the older value)
        firePropertyChange(BeanValidator.VALID_PROPERTY, null, valid);
    }


    public String getContext() {
        return getModel().getContext();
    }

    public void setContext(String context) {

        String oldContext = getContext();

        if (Objects.equals(context, oldContext)) {
            // same context do nothing
            return;
        }

        NuitonValidatorModel<O> model = getModel();

        // compute the new validator model
        NuitonValidatorScope[] scopes = model.getScopes().toArray(new NuitonValidatorScope[0]);

        rebuildDelegateValidator(model.getType(), context, scopes);

        firePropertyChange(BeanValidator.CONTEXT_PROPERTY, oldContext, context);
    }

    public Set<NuitonValidatorScope> getScopes() {
        return getModel().getScopes();
    }

    public void setScopes(NuitonValidatorScope... scopes) {

        Set<NuitonValidatorScope> oldScopes = getScopes();

        rebuildDelegateValidator(getModel().getType(), getModel().getContext(), scopes);

        firePropertyChange(BeanValidator.SCOPES_PROPERTY, oldScopes, scopes);
    }

    public Set<NuitonValidatorScope> getEffectiveScopes() {
        return getDelegate().getEffectiveScopes();
    }

    public Set<String> getEffectiveFields() {
        return getDelegate().getEffectiveFields();
    }

    public Set<String> getEffectiveFields(NuitonValidatorScope scope) {
        return getDelegate().getEffectiveFields(scope);
    }

    public Class<O> getType() {
        return getModel().getType();
    }

    /**
     * Test a the validator contains the field given his name
     *
     * @param fieldName the name of the searched field
     * @return <code>true</code> if validator contains this field, <code>false</code> otherwise
     */
    public boolean containsField(String fieldName) {
        Set<String> effectiveFields = getDelegate().getEffectiveFields();
        return effectiveFields.contains(fieldName);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    protected NuitonValidatorModel<O> getModel() {
        return getDelegate().getModel();
    }

    protected void doValidate(O bean) {
        if (!Objects.equals(bean, getBean())) {
            throw new IllegalStateException(String.format("Can not validate the bean [%s] which is not the one registered [%s] in this validator.", bean, bean));
        }
        doValidate();
    }

    protected NuitonValidator<O> getDelegate() {
        return context.getValidator();
    }

    protected void rebuildDelegateValidator(Class<O> beanType, String context, NuitonValidatorScope... scopes) {

        // changing context could change fields definition
        // so detach bean, must rebuild the fields

        // Detach the bean before any thing, because with the new delegate
        // validator some old fields could not be used any longer, and then
        // listeners will never have the full reset of their model...
        if (getBean() != null) {
            setBean(null);
        }

        if (scopes == null || scopes.length == 0) {
            scopes = NuitonValidatorScope.values();
        }

        // compute the new validator model
        NuitonValidatorModel<O> validatorModel = validatorProvider.getModelProvider().getModel(beanType, context, scopes);

        // remove old delegate validator
        NuitonValidator<O> delegate = validatorProvider.newValidator(validatorModel);
        this.context.setValidator(delegate);
    }

    /**
     * il faut eviter le code re-intrant (durant une validation, une autre est
     * demandee). Pour cela on fait la validation dans un thread, et tant que la
     * premiere validation n'est pas fini, on ne repond pas aux solicitations.
     * Cette method est public pour permettre de force une validation par
     * programmation, ce qui est utile par exemple si le bean ne supporte pas
     * les {@link PropertyChangeListener}
     *
     * <b>Note:</b> la methode est protected et on utilise la methode
     * {@link #doValidate()} car la méthode ne modifie pas les etats
     * internes et cela en rend son utilisation delicate (le validateur entre
     * dans un etat incoherent par rapport aux messages envoyés).
     */
    protected void validate() {

        if (isCanValidate()) {

            NuitonValidatorResult result = context.validate();

            mergeMessages(result);

            if (parentValidator != null) {
                // chained validation
                // the parent validator should not be changed from this validation
                boolean wasModified = parentValidator.isChanged();
                parentValidator.doValidate();
                if (!wasModified) {
                    // push back old state
                    parentValidator.setChanged(false);
                }
            }
        }
    }

    protected void fireFieldChanged(BeanValidatorEvent<O> evt) {

        for (BeanValidatorListener listener : listenerList.getListeners(BeanValidatorListener.class)) {
            listener.onFieldChanged(evt);
        }
    }

    protected void mergeMessages(NuitonValidatorResult newMessages) {

        List<BeanValidatorEvent<O>> events = context.mergeMessages(this, newMessages);
        if (events != null) {
            // send all messages
            for (BeanValidatorEvent<O> event : events) {
                fireFieldChanged(event);
            }
        }
    }
}
