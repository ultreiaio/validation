/*
 * #%L
 * Validation :: Bean
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.validation.bean;

import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * The object to box a validation message.
 *
 * @param <E> type of message (use for override {@link #compareTo(BeanValidatorMessage)} method.
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public class BeanValidatorMessage<E extends BeanValidatorMessage<E>> implements Comparable<E>, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * the validator that produce the message
     */
    protected BeanValidator<?> validator;

    /**
     * the field that produce the message
     */
    protected String field;

    /**
     * the label of the message (to be displayed somewhere)
     */
    protected String message;

    /**
     * the scope of the message
     */
    protected NuitonValidatorScope scope;

    public BeanValidatorMessage(BeanValidator<?> validator,
                                String field,
                                String message,
                                NuitonValidatorScope scope) {
        this.field = field;
        this.validator = validator;
        this.message = message == null ? null : message.trim();
        this.scope = scope;
    }

    public BeanValidator<?> getValidator() {
        return validator;
    }

    public String getField() {
        return field;
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public int compareTo(E o) {
        // sort on scope
        int result = getScope().compareTo(o.getScope());
        if (result == 0) {
            // sort on field name
            result = field.compareTo(o.field);
            if (result == 0) {
                // sort on message
                result = message.compareTo(o.message);
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BeanValidatorMessage<?>)) {
            return false;
        }

        BeanValidatorMessage<?> that = (BeanValidatorMessage<?>) o;

        return field.equals(that.field) &&
                (message != null ? !message.equals(that.message) : that.message == null) &&
                scope == that.scope;
    }

    @Override
    public int hashCode() {
        int result = field.hashCode();
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (scope != null ? scope.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return scope + " - " + getI18nError(message);
    }

    public String getI18nError(String error) {
        String text;
        if (!error.contains("##")) {
            text = t(error);
        } else {
            StringTokenizer stk = new StringTokenizer(error, "##");
            String errorName = stk.nextToken();
            List<String> args = new ArrayList<>();
            while (stk.hasMoreTokens()) {
                args.add(stk.nextToken());
            }
            text = t(errorName, args.toArray());
        }
        return text;
    }
}
