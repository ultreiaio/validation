package io.ultreia.java4all.validation.bean;
/*
 * #%L
 * Validation :: Bean
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.util.EventObject;

/**
 * Event to be fired when some messages changed on a given field / scope of a bean.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public class BeanValidatorEvent<V> extends EventObject {
    private static final long serialVersionUID = 1L;

    /**
     * the field impacted by the validator
     */
    protected String field;

    /**
     * the scope impacted by the event
     */
    protected NuitonValidatorScope scope;

    protected String[] messagesToAdd;

    protected String[] messagesToDelete;

    public BeanValidatorEvent(BeanValidator<V> source,
                              String field,
                              NuitonValidatorScope scope,
                              String[] messagesToAdd,
                              String[] messagesToDelete) {
        super(source);
        this.field = field;
        this.scope = scope;
        this.messagesToAdd = messagesToAdd;
        this.messagesToDelete = messagesToDelete;
    }

    public V getBean() {
        return getSource().getBean();
    }

    @SuppressWarnings("unchecked")
    @Override
    public BeanValidator<V> getSource() {
        return (BeanValidator<V>) super.getSource();
    }

    public String[] getMessagesToAdd() {
        return messagesToAdd;
    }

    public String[] getMessagesToDelete() {
        return messagesToDelete;
    }

    public NuitonValidatorScope getScope() {
        return scope;
    }

    public String getField() {
        return field;
    }
}
