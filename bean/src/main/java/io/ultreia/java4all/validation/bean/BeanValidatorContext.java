package io.ultreia.java4all.validation.bean;

/*-
 * #%L
 * Validation :: Bean
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.api.NuitonValidator;
import io.ultreia.java4all.validation.api.NuitonValidatorResult;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 26/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class BeanValidatorContext<O> {
    private static final Logger log = LogManager.getLogger(BeanValidatorContext.class);

    /**
     * map of conversion errors detected by this validator
     */
    protected final Map<String, String> conversionErrors;
    /**
     * Optional validation context.
     */
    protected final NuitonValidationContext validationContext;
    /**
     * Bean to validate.
     */
    protected O bean;
    /**
     * State of validation (keep all messages of validation for the filled
     * bean).
     */
    protected NuitonValidatorResult messages;
    /**
     * Validator.
     */
    protected NuitonValidator<O> validator;
    /**
     * State to know if the validator can be used (we keep this state for
     * performance reasons : do not want to compute this value each time a
     * validation is asked...).
     */
    protected boolean canValidate;

    public BeanValidatorContext(NuitonValidationContext validationContext) {
        this.validationContext = validationContext;
        conversionErrors = new TreeMap<>();
    }


    public O getBean() {
        return bean;
    }

    public void setBean(O bean) {
        if (log.isDebugEnabled()) {
            log.debug(this + " : " + bean);
        }

        // clean conversions of previous bean
        conversionErrors.clear();
        this.bean = bean;

        setCanValidate(!validator.getEffectiveFields().isEmpty() && bean != null);
    }

    protected BeanValidatorEvent<O> createEvent(BeanValidator<O> source,
                                                String field,
                                                NuitonValidatorScope scope,
                                                String[] toAdd,
                                                String[] toDelete) {
        return new BeanValidatorEvent<>(
                source,
                field,
                scope,
                toAdd,
                toDelete
        );
    }

    public NuitonValidator<O> getValidator() {
        return validator;
    }

    public void setValidator(NuitonValidator<O> validator) {
        this.validator = validator;
    }

    public NuitonValidatorResult getMessages() {
        return messages;
    }

    public boolean isCanValidate() {
        return canValidate;
    }

    public void setCanValidate(boolean canValidate) {
        this.canValidate = canValidate;
    }

    public boolean isValid() {
        return messages == null || messages.isValid();
    }

    public boolean hasFatalErrors() {
        return messages != null && messages.hasFatalMessages();
    }

    public boolean hasErrors() {
        return messages != null && messages.hasErrorMessagess();
    }

    public boolean hasWarnings() {
        return messages != null && messages.hasWarningMessages();
    }

    public boolean hasInfos() {
        return messages != null && messages.hasInfoMessages();
    }

    public boolean isValid(String fieldName) {

        // field is valid if no fatal messages nor error messages

        return !(
                messages.hasMessagesForScope(fieldName, NuitonValidatorScope.FATAL) ||
                        messages.hasMessagesForScope(fieldName, NuitonValidatorScope.ERROR));
    }

    public NuitonValidatorScope getHighestScope(String field) {

        return messages.getFieldHighestScope(field);
    }

    public NuitonValidatorResult validate() {
        NuitonValidatorResult result = validator.validate(bean, validationContext);

        // treat conversion errors and re-inject them
        for (Map.Entry<String, String> entry : conversionErrors.entrySet()) {
            // remove from validation, errors occurs on this field
            String field = entry.getKey();
            List<String> errors = result.getErrorMessages(field);
            String conversionError = entry.getValue();
            if (errors != null) {
                errors.clear();
                errors.add(conversionError);
            } else {
                errors = Collections.singletonList(conversionError);
            }
            result.setMessagesForScope(NuitonValidatorScope.ERROR, field, errors);
        }
        return result;
    }

    public List<BeanValidatorEvent<O>> mergeMessages(BeanValidator<O> beanValidator, NuitonValidatorResult newMessages) {
        if (newMessages == null && messages == null) {
            // no messages ever registered and ask to delete them, so nothing to do
            return null;
        }
        Set<NuitonValidatorScope> scopes = getValidator().getEffectiveScopes();
        // list of events to send after the merge of messages
        List<BeanValidatorEvent<O>> events = new LinkedList<>();
        for (NuitonValidatorScope scope : scopes) {
            // do the merge at scope level
            mergeMessages(beanValidator, scope, newMessages, events);
        }
        if (newMessages != null) {
            //TODO tchemit 2011-01-23 Perhaps it will necessary to clear the messages for memory performance ?
            // finally keep the new messages as the current messages
            this.messages = newMessages;
        }
        return events;
    }

    protected void mergeMessages(BeanValidator<O> beanValidator,
                                 NuitonValidatorScope scope,
                                 NuitonValidatorResult newMessages,
                                 List<BeanValidatorEvent<O>> events) {
        if (newMessages == null) {
            // special case to empty all messages
            List<String> fieldsForScope = messages.getFieldsForScope(scope);
            for (String field : fieldsForScope) {
                List<String> messagesForScope = messages.getMessagesForScope(field, scope);
                events.add(createEvent(beanValidator, field, scope, null, messagesForScope.toArray(new String[0])));
            }
            // suppress all messages for this scope
            messages.clearMessagesForScope(scope);
        } else {
            List<String> newFields = newMessages.getFieldsForScope(scope);
            if (messages == null) {
                // first time of a merge, just add new messages
                for (String field : newFields) {
                    List<String> messagesForScope = newMessages.getMessagesForScope(field, scope);
                    events.add(createEvent(beanValidator, field, scope, messagesForScope.toArray(new String[0]), null));
                }
                // nothing else to do
                return;
            }
            List<String> oldFields = messages.getFieldsForScope(scope);
            Iterator<String> itr;
            // detects field with only new messages
            itr = newFields.iterator();
            while (itr.hasNext()) {
                String newField = itr.next();
                if (!oldFields.contains(newField)) {
                    // this fields has now messages but not before : new messages
                    List<String> messagesForScope = newMessages.getMessagesForScope(newField, scope);
                    events.add(createEvent(beanValidator, newField, scope, messagesForScope.toArray(new String[0]), null));
                    // treated field
                    itr.remove();
                }
            }
            // detects fields with only obsolete messages
            itr = oldFields.iterator();
            while (itr.hasNext()) {
                String oldField = itr.next();
                if (!newFields.contains(oldField)) {
                    // this fields has no more messages
                    List<String> messagesForScope = messages.getMessagesForScope(oldField, scope);
                    events.add(createEvent(beanValidator, oldField, scope, null, messagesForScope.toArray(new String[0])));
                    // treated field
                    itr.remove();
                }
            }
            // now deal with mixing field (toAdd and toDelete)
            for (String field : newFields) {
                List<String> newMessagesForScope = newMessages.getMessagesForScope(field, scope);
                List<String> oldMessagesForScope = messages.getMessagesForScope(field, scope);
                // get old obsoletes messages to delete
                Set<String> toDelete = new HashSet<>(oldMessagesForScope);
                newMessagesForScope.forEach(toDelete::remove);
                // get new messages to add
                Set<String> toAdd = new HashSet<>(newMessagesForScope);
                oldMessagesForScope.forEach(toAdd::remove);
                events.add(createEvent(
                        beanValidator,
                        field,
                        scope,
                        toAdd.isEmpty() ? null : toAdd.toArray(new String[0]),
                        toDelete.isEmpty() ? null : toDelete.toArray(new String[0])
                ));
            }
        }
    }
}
