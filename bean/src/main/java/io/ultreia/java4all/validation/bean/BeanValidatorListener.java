package io.ultreia.java4all.validation.bean;
/*
 * #%L
 * Validation :: Bean
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.EventListener;

/**
 * The definition of an event on {@link BeanValidatorEvent}
 * to be fired by a {@link BeanValidator}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5.2
 */
public interface BeanValidatorListener extends EventListener {


    /**
     * Invoked when the {@link BeanValidatorEvent} detects some changes for a
     * given bean / field / scope.
     *
     * @param event the event
     */
    void onFieldChanged(BeanValidatorEvent<?> event);
}
