/*
 * #%L
 * Validation :: Bean
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/**
 * Package of Nuiton - Bean Validator api.
 *
 * <h2>The <b>BeanValidator</b> api</h2>
 * <p>
 * The {@link io.ultreia.java4all.validation.bean.BeanValidator} purpose is to validate
 * a bean, with a listener api to interact with outside world.
 * </p>
 * It is mainly used in GUI parts of an application (Jaxx-validator use it).
 * <p>
 * The idea is to attach the bean to validate inside the validator, then the
 * validator listen any modification of the bean to revalidate it and fires
 * events when messages has changed on a field.
 * <pre>
 * BeanValidatorListener listener = new BeanValidatorListener() {XXX};
 * BeanValidator&lt;O&gt; validator = XXX;
 * validator.addSimpleBeanValidatorListener(listener);
 * validator.setBean(o);
 * </pre>
 * <h3>Obtain a validator</h3>
 * To obtain a bean validator use the factory method of the
 * {@link io.ultreia.java4all.validation.bean.BeanValidator}.
 * <pre>
 * BeanValidator&lt;O&gt; validator = BeanValidator.newValidator(...);
 * </pre>
 *
 * @since 2.0
 */
package io.ultreia.java4all.validation.bean;
