package io.ultreia.java4all.validation.bean;

/*-
 * #%L
 * Validation :: Bean
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A dictionary of validators indexed by the bean type.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonValidatorsMap {

    private final Map<Class<?>, BeanValidator<?>> delegate;

    private NuitonValidatorsMap() {
        delegate = new HashMap<>();
    }

    public static Builder create(NuitonValidatorProvider validatorProvider, Set<? extends Class<?>> types, String context, Collection<NuitonValidatorScope> scopes) {
        return new Builder(validatorProvider, context, scopes, types);
    }

    public NuitonValidatorScope[] getScopes() {
        return delegate.values().stream().flatMap(s -> s.getScopes().stream()).distinct().toArray(NuitonValidatorScope[]::new);
    }

    @SuppressWarnings("unchecked")
    public <X> BeanValidator<X> getValidator(Class<X> klass) {
        return (BeanValidator<X>) get(klass);
    }

    public int size() {
        return delegate.size();
    }

    public boolean isEmpty() {
        return delegate.isEmpty();
    }

    public boolean containsKey(Class<?> key) {
        return delegate.containsKey(key);
    }

    public BeanValidator<?> get(Class<?> key) {
        return delegate.get(key);
    }

    public BeanValidator<?> put(Class<?> key, BeanValidator<?> value) {
        return delegate.put(key, value);
    }

    public Collection<BeanValidator<?>> values() {
        return delegate.values();
    }

    public Set<Class<?>> keySet() {
        return delegate.keySet();
    }

    public static class Builder {

        private final NuitonValidatorProvider validatorProvider;
        private final String context;
        private final NuitonValidatorScope[] scopes;
        private final Set<? extends Class<?>> types;

        private Builder(NuitonValidatorProvider validatorProvider, String context, Collection<NuitonValidatorScope> scopes, Set<? extends Class<?>> types) {
            this.validatorProvider = validatorProvider;
            this.context = context;
            this.scopes = scopes.toArray(new NuitonValidatorScope[0]);
            this.types = types;
        }

        public NuitonValidatorsMap build() {
            NuitonValidatorsMap result = new NuitonValidatorsMap();
            for (Class<?> type : types) {
                BeanValidator<?> validator = getValidator(type);
                if (validator == null) {
                    continue;
                }
                result.put(type, validator);
            }
            return result;
        }

        private <B> BeanValidator<B> getValidator(Class<B> type) {
            BeanValidator<B> validator = new BeanValidator<>(validatorProvider, type, context, scopes);
            Set<NuitonValidatorScope> effectiveScopes = validator.getEffectiveScopes();
            if (effectiveScopes.isEmpty()) {
                validator = null;
            }
            return validator;
        }
    }
}
