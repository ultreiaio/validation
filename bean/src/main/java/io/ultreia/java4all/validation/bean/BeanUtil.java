package io.ultreia.java4all.validation.bean;

/*-
 * #%L
 * Validation :: Bean
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;

/**
 * Created on 03/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.3
 */
public class BeanUtil {

    public static final String ADD_PROPERTY_CHANGE_LISTENER = "addPropertyChangeListener";

    public static final String REMOVE_PROPERTY_CHANGE_LISTENER = "removePropertyChangeListener";


    /**
     * Test if the given type is JavaBean compliant, says that it has two
     * public methods :
     * <ul>
     * <li>{@code addPropertyChangeListener}</li>
     * <li>{@code removePropertyChangeListener}</li>
     * </ul>
     *
     * @param type type to test
     * @return {@code true} if type is Javabean compliant, {@code false} otherwise
     * @since 2.0
     */
    public static boolean isJavaBeanCompliant(Class<?> type) {

        try {
            type.getMethod(ADD_PROPERTY_CHANGE_LISTENER, PropertyChangeListener.class);
        } catch (NoSuchMethodException e) {
            // no add method
            return false;
        }

        try {
            type.getMethod(REMOVE_PROPERTY_CHANGE_LISTENER, PropertyChangeListener.class);
        } catch (NoSuchMethodException e) {
            // no add method
            return false;
        }

        return true;
    }


    /**
     * Add the given {@code listener} to the given {@code bean} using the
     * normalized method named {@code addPropertyChangeListener}.
     *
     * @param listener the listener to add
     * @param bean     the bean on which the listener is added
     * @throws InvocationTargetException if could not invoke the method
     *                                   {@code addPropertyChangeListener}
     * @throws NoSuchMethodException     if method
     *                                   {@code addPropertyChangeListener}
     *                                   does not exist on given bean
     * @throws IllegalAccessException    if an illegal access occurs when
     *                                   invoking the method
     *                                   {@code addPropertyChangeListener}
     */
    public static void addPropertyChangeListener(PropertyChangeListener listener,
                                                 Object bean) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        bean.getClass().getMethod(ADD_PROPERTY_CHANGE_LISTENER, PropertyChangeListener.class).invoke(bean, listener);
    }

    /**
     * Remove the given {@code listener} from the given {@code bean} using the
     * normalized method named {@code removePropertyChangeListener}.
     *
     * @param listener the listener to remove
     * @param bean     the bean on which the listener is removed
     * @throws InvocationTargetException if could not invoke the method
     *                                   {@code removePropertyChangeListener}
     * @throws NoSuchMethodException     if method
     *                                   {@code removePropertyChangeListener}
     *                                   does not exist on given bean
     * @throws IllegalAccessException    if an illegal access occurs when
     *                                   invoking the method
     *                                   {@code removePropertyChangeListener}
     */
    public static void removePropertyChangeListener(PropertyChangeListener listener,
                                                    Object bean) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {

        bean.getClass().getMethod(REMOVE_PROPERTY_CHANGE_LISTENER, PropertyChangeListener.class).invoke(bean, listener);
    }

}
