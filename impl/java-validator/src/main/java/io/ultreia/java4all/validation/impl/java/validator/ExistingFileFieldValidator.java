package io.ultreia.java4all.validation.impl.java.validator;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;

import java.io.File;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * ExistingFileFieldValidator checks that a File field exists.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class ExistingFileFieldValidator<O> extends SkipableFieldValidatorSupport<O, File> {
    public ExistingFileFieldValidator(String fieldName, Function<O, File> fieldFunction, String messageKey, MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder, BiFunction<O, NuitonValidationContext, Boolean> skipFunction) {
        super(fieldName, fieldFunction, messageKey, messageBuilder, skipFunction);
    }

    @Override
    public void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        File value = getField(object);
        if (value == null) {
            // no value defined
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
            return;
        }

        if (!(value.isFile() && value.exists())) {
            // f is  not a file nor exists
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
        }
    }
}
