package io.ultreia.java4all.validation.impl.java.validator;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;

import java.lang.reflect.TypeVariable;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 * Created on 26/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public abstract class FieldValidatorSupport<O, F> implements FieldValidator<O, F> {

    private final String fieldName;
    private final Function<O, F> fieldFunction;
    private final String messageKey;
    private final MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder;

    public FieldValidatorSupport(String fieldName, Function<O, F> fieldFunction, String messageKey, MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder) {
        this.fieldName = Objects.requireNonNull(fieldName);
        this.fieldFunction = fieldFunction;
        this.messageKey = messageKey;
        this.messageBuilder = messageBuilder;
    }

    public FieldValidatorSupport(String fieldName, Function<O, F> fieldFunction) {
        this.fieldName = Objects.requireNonNull(fieldName);
        this.fieldFunction = fieldFunction;
        this.messageKey = null;
        this.messageBuilder = null;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public F getField(O object) {
        return Objects.requireNonNull(fieldFunction()).apply(object);
    }

    @Override
    public String getMessage(O object, NuitonValidationContext validationContext) {
        if (messageKey == null) {
            return null;
        }
        if (messageBuilder == null) {
            return I18n.l(validationContext.getLocale(), messageKey);
        }
        return messageBuilder.build(object, validationContext, this, messageKey);
    }

    protected Function<O, F> fieldFunction() {
        return fieldFunction;
    }

    public static class GeneratorSupport implements FieldValidatorGenerator {

        private final Class<?> validatorType;
        private final boolean addFieldName;
        private final boolean addFieldFunction;
        private final boolean addMessageKey;
        private final boolean addMessageBuilder;

        protected GeneratorSupport(Class<?> validatorType, boolean addFieldName, boolean addFieldFunction, boolean addMessageKey, boolean addMessageBuilder) {
            this.validatorType = validatorType;
            this.addFieldName = addFieldName;
            this.addFieldFunction = addFieldFunction;
            this.addMessageKey = addMessageKey;
            this.addMessageBuilder = addMessageBuilder;
        }

        protected GeneratorSupport(Class<?> validatorType) {
            this(validatorType, true, true, true, true);
        }

        public static String escapeFloat(String str) {
            return String.format("%sf", str);
        }

        @Override
        public boolean accept(Class<?> validationType) {
            return validatorType.equals(validationType);
        }

        @Override
        public final String generate(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager) throws NoSuchMethodException {
            importManager.addImport(validationContextType);
            List<String> parameters = generateParameters(key, validatorDefinition, validationContextType, importManager);
            generateExtraParameters(key, validatorDefinition, validationContextType, importManager, parameters);
            TypeVariable<? extends Class<? extends FieldValidator<?, ?>>>[] typeParameters = validatorDefinition.getValidator().getTypeParameters();
            boolean addGeneric = typeParameters.length > 0;
            return String.format("() -> new %s%s(%s)", importManager.addImport(validatorDefinition.getValidator()), addGeneric ? "<>" : "", String.join(", ", parameters));
        }

        protected List<String> generateParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager) throws NoSuchMethodException {
            List<String> result = new LinkedList<>();
            if (addFieldName) {
                // add fieldName
                addFieldNameParameter(validatorDefinition, result);
            }
            if (addFieldFunction) {
                // add fieldFunction
                addFieldFunctionParameter(key, validatorDefinition, result);
            }
            if (addMessageKey) {
                // add message key
                addMessageKey(validatorDefinition, importManager, result);
            }
            if (addMessageBuilder) {
                // add message function
                addMessageFunctionParameter(key, validatorDefinition, validationContextType, importManager, result);
            }
            return result;
        }

        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            // by default nothing more to add here
        }

        protected Class<?> getValidatorType() {
            return validatorType;
        }

        protected ExpressionResolver validatorResolver(Class<?> beanType, Class<?> validationContextType, Class<? extends FieldValidator<?, ?>> validatorType) {
            return ExpressionResolver.of(new ExpressionResolver.Config("$c", "c", validationContextType, true),
                                         new ExpressionResolver.Config("$o", "o", beanType, false),
                                         new ExpressionResolver.Config("$v", "v", validatorType, true));
        }

        protected ExpressionResolver validationContextResolver(Class<?> validationContextType) {
            return ExpressionResolver.of(new ExpressionResolver.Config("$c", "c", validationContextType, true));
        }

        protected ExpressionResolver beanAndValidationContextResolver(Class<?> beanType, Class<?> validationContextType) {
            return ExpressionResolver.of(new ExpressionResolver.Config("$o", "o", beanType, false), new ExpressionResolver.Config("$c", "c", validationContextType, true));
        }

        private void addFieldNameParameter(FieldValidatorDefinition validatorDefinition, List<String> result) {
            String fieldName = validatorDefinition.getFieldName();
            result.add(ExpressionResolver.escapeString(fieldName));
        }

        private void addFieldFunctionParameter(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, List<String> result) throws NoSuchMethodException {
            String fieldName = validatorDefinition.getFieldName();
            Class<?> beanType = key.getBeanType();
            String getterName = ExpressionResolver.guessGetterName(beanType, fieldName);
            result.add(String.format("%s::%s", beanType.getSimpleName(), getterName));
        }

        private void addMessageKey(FieldValidatorDefinition validatorDefinition, ImportManager importManager, List<String> result) {
            if (validatorDefinition.withoutMessage()) {
                // no message key parameter
                result.add("null");
                return;
            }
            importManager.addImport(I18n.class);
            String message = validatorDefinition.getMessage();
            int endIndex = message.indexOf("##");
            if (endIndex == -1) {
                result.add(String.format("I18n.n(%s)", ExpressionResolver.escapeString(message)));
                return;
            }
            String messageKey = message.substring(0, endIndex);
            result.add(String.format("I18n.n(%s)", ExpressionResolver.escapeString(messageKey)));
        }

        private void addMessageFunctionParameter(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            if (validatorDefinition.withoutMessage()) {
                // no message parameter
                result.add("null");
                return;
            }
            String message = validatorDefinition.getMessage();
            int endIndex = message.indexOf("##");
            if (endIndex == -1) {
                result.add("null");
                return;
            }
            ExpressionResolver resolver = validatorResolver(key.getBeanType(), validationContextType, validatorDefinition.getValidator());
            List<String> parameters = new LinkedList<>();
            for (String parameter : message.substring(endIndex + 2).split("##")) {
                parameters.add(resolver.resolve(parameter));
            }
            importManager.addImport(I18n.class);
            String call = String.format("(o, c, v, k) -> I18n.l(c.getLocale(), k, %s)", String.join(", ", parameters));
            result.add(call);
        }
    }


}
