package io.ultreia.java4all.validation.impl.java.validator;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Function;

/**
 * Copy from com.opensymphony.xwork2.validator.validators.URLValidator (no time to do better).
 * <p>
 * Created on 11/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 6.0.6
 */
public class CheckUrl<O> extends FieldValidatorSupport<O, String> {
    private static final Logger log = LogManager.getLogger(CheckUrl.class);

    public CheckUrl(String fieldName,
                    Function<O, String> fieldFunction,
                    String messageKey,
                    MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder) {
        super(fieldName, fieldFunction, messageKey, messageBuilder);
    }

    public static boolean verifyUrl(String url) {
        if (log.isDebugEnabled()) {
            log.debug("Checking if url [{}] is valid", url);
        }
        if (url == null) {
            return false;
        }

        if (url.startsWith("https://")) {
            // URL doesn't understand the https protocol, hack it
            url = "http://" + url.substring(8);
        }

        try {
            new URL(url);

            return true;
        } catch (MalformedURLException e) {
            if (log.isDebugEnabled()) {
                log.debug("Url [{}] is invalid: {}", url, e.getMessage(), e);
            }
            return false;
        }
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        String value = getField(object);

        // if there is no value - don't do comparison
        // if a value is required, a required validator should be added to the field
        if (value == null || value.isEmpty()) {
            return;
        }

        // FIXME deprecated! the same regex below should be used instead
        // replace logic with next major release
        if (!verifyUrl(value)) {
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
        }
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(CheckUrl.class);
        }

    }

}
