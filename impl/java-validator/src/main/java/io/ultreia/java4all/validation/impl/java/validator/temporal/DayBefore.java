package io.ultreia.java4all.validation.impl.java.validator.temporal;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.Dates;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;

import java.util.Date;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * To check that the field name date is before another given field ({@code endDateProperty}).
 * <p>
 * Created on 11/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.24
 */
public class DayBefore<O> extends DateBeforeSupport<O> {

    public DayBefore(String fieldName,
                     Function<O, Date> fieldFunction,
                     String messageKey,
                     MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder,
                     BiFunction<O, NuitonValidationContext, Boolean> skipFunction,
                     boolean strict,
                     BiFunction<O, NuitonValidationContext, Date> otherDateFunction,
                     Map<String, Object> parameters) {
        super(fieldName,
              fieldFunction,
              messageKey == null ? I18n.n("observe.Common.validation.startDate.before.endDate") : messageKey,
              messageBuilder,
              skipFunction,
              strict,
              "%1$td/%1$tm/%1$tY",
              otherDateFunction,
              parameters);
    }

    @Override
    protected Date format(Date date) {
        return Dates.getDay(date);
    }
}
