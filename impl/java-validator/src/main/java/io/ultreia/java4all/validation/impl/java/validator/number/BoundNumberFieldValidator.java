package io.ultreia.java4all.validation.impl.java.validator.number;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created by tchemit on 19/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BoundNumberFieldValidator<O, F extends Number> extends SkipableFieldValidatorSupport<O, F> {

    private final float min;
    private final float max;

    public BoundNumberFieldValidator(String fieldName, Function<O, F> fieldFunction, BiFunction<O, NuitonValidationContext, Boolean> skipFunction, float min, float max) {
        super(fieldName, fieldFunction, skipFunction);
        this.min = min;
        this.max = max;
    }

    public float getMin() {
        return min;
    }

    public float getMax() {
        return max;
    }


    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        Number fieldValue = getField(object);
        if (fieldValue == null) {
            return;
        }
        float doubleValue = fieldValue.floatValue();
        if (doubleValue >= min && max >= doubleValue) {
            return;
        }
        addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.number.bound"), min, max, doubleValue);
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(BoundNumberFieldValidator.class, true, true, false, false, true);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(escapeFloat(validatorDefinition.getParameter("min")));
            result.add(escapeFloat(validatorDefinition.getParameter("max")));
        }
    }
}
