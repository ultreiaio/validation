package io.ultreia.java4all.validation.impl.java.validator.string;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * To validate a field that is a regex.
 * <p>
 * Created on 05/10/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.49
 */
public class RegexFieldValidator<O> extends FieldValidatorSupport<O, String> {

    public RegexFieldValidator(String fieldName, Function<O, String> fieldFunction) {
        super(fieldName, fieldFunction);
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        String regex = getField(object);
        if (regex == null) {
            return;
        }
        regex = regex.trim();
        if (regex.isBlank()) {
            return;
        }
        try {
            Pattern.compile(regex);
        } catch (PatternSyntaxException e) {
            addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.regex.syntax.invalid"), regex);
        }
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(RegexFieldValidator.class, true, true, false, false);
        }
    }

}

