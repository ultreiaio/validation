package io.ultreia.java4all.validation.impl.java.validator;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.FieldValidatorFunction;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created on 28/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class FieldExpressionValidator<O, F> extends SkipableFieldValidatorSupport<O, F> {
    private final FieldValidatorFunction<O, ? super NuitonValidationContext, ? super FieldExpressionValidator<O, ?>, Boolean> expressionFunction;

    public FieldExpressionValidator(String fieldName,
                                    Function<O, F> fieldFunction,
                                    String messageKey,
                                    MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder,
                                    BiFunction<O, NuitonValidationContext, Boolean> skipFunction,
                                    FieldValidatorFunction<O, ? super NuitonValidationContext, ? super FieldExpressionValidator<O, ?>, Boolean> expressionFunction) {
        super(fieldName, fieldFunction, messageKey, messageBuilder, skipFunction);
        this.expressionFunction = expressionFunction;
    }

    public FieldExpressionValidator(String fieldName,
                                    Function<O, F> fieldFunction,
                                    BiFunction<O, NuitonValidationContext, Boolean> skipFunction,
                                    FieldValidatorFunction<O, ? super NuitonValidationContext, ? super FieldExpressionValidator<O, ?>, Boolean> expressionFunction) {
        this(fieldName, fieldFunction, null, null, skipFunction, expressionFunction);
    }

    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        boolean valid = evaluateExpressionParameter(object, validationContext);
        if (!valid) {
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
        }
    }

    protected boolean evaluateExpressionParameter(O object, NuitonValidationContext validationContext) {
        return expressionFunction.apply(object, validationContext, this);
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {

        public Generator() {
            super(FieldExpressionValidator.class);
        }

        protected Generator(Class<?> validatorType) {
            super(validatorType);
        }

        @Override
        protected final List<String> generateParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager) throws NoSuchMethodException {
            List<String> parameters = super.generateParameters(key, validatorDefinition, validationContextType, importManager);
            addExpressionFunctionParameter(key, validatorDefinition, validationContextType, parameters);
            return parameters;
        }

        protected final void addExpressionFunctionParameter(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, List<String> result) throws NoSuchMethodException {
            Optional<String> optionalExpression = validatorDefinition.getOptionalParameter("expression");
            if (optionalExpression.isEmpty()) {
                result.add("null");
                return;
            }
            String call = "(o, c, v) -> " + validatorResolver(key.getBeanType(), validationContextType, validatorDefinition.getValidator()).resolve(optionalExpression.get().trim());
            result.add(call);
        }
    }

}
