package io.ultreia.java4all.validation.impl.java.validator.string;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.function.Function;

/**
 * Created by tchemit on 17/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class StringMaxLength<O> extends FieldValidatorSupport<O, String> {

    private static final Logger log = LogManager.getLogger(StringMaxLength.class);
    private final Integer maxLength;

    public StringMaxLength(String fieldName, Function<O, String> fieldFunction, Integer maxLength) {
        super(fieldName, fieldFunction);
        this.maxLength = maxLength;
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        String fieldValue = getField(object);
        if (fieldValue == null || fieldValue.length() <= maxLength) {
            return;
        }
        int length = fieldValue.length();
        log.debug(String.format("[%s] Found a comment too big (%s).", getFieldName(), length));
        addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.string.too.big"), maxLength, length);
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(StringMaxLength.class, true, true, false, false);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(validatorDefinition.getParameter("maxLength"));
        }
    }
}
