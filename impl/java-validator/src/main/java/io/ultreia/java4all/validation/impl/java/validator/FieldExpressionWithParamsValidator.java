/*
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.validation.impl.java.validator;

import com.google.auto.service.AutoService;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.FieldValidatorFunction;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Extends {@link FieldExpressionValidator} to add some extra parameters available
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class FieldExpressionWithParamsValidator<O, F> extends FieldExpressionValidator<O, F> {

    protected static final Pattern EXTRA_BOOLEAN_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+):(false|true)");
    protected static final Pattern EXTRA_SHORT_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+):(-\\d+|\\d+)");
    protected static final Pattern EXTRA_INT_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+):(-\\d+|\\d+)");
    protected static final Pattern EXTRA_LONG_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+):(-\\d+|\\d+)");
    protected static final Pattern EXTRA_DOUBLE_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+):(-\\d+\\.\\d+|\\d+\\.\\d+)");
    protected static final Pattern EXTRA_STRING_PARAM_ENTRY_PATTERN = Pattern.compile("(\\w+):(.+)");
    private static final Logger log = LogManager.getLogger(FieldExpressionWithParamsValidator.class);

    private final Map<String, Boolean> booleans;

    private final Map<String, Short> shorts;

    private final Map<String, Integer> ints;

    private final Map<String, Long> longs;

    private final Map<String, Double> doubles;

    private final Map<String, String> strings;


    public FieldExpressionWithParamsValidator(String fieldName,
                                              Function<O, F> fieldFunction,
                                              String messageKey,
                                              MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageFunction,
                                              BiFunction<O, NuitonValidationContext, Boolean> skipFunction,
                                              FieldValidatorFunction<O, ? super NuitonValidationContext, ? super FieldExpressionValidator<O, ?>, Boolean> expressionFunction,
                                              Map<String, Object> parameters) {
        super(fieldName, fieldFunction, messageKey, messageFunction, skipFunction, expressionFunction);
        String booleanParams = (String) parameters.get("booleans");
        String shortParams = (String) parameters.get("shorts");
        String intParams = (String) parameters.get("ints");
        String longParams = (String) parameters.get("longs");
        String doubleParams = (String) parameters.get("doubles");
        String stringParams = (String) parameters.get("strings");

        booleans = initParams(Boolean.class, booleanParams, EXTRA_BOOLEAN_PARAM_ENTRY_PATTERN);
        shorts = initParams(Short.class, shortParams, EXTRA_SHORT_PARAM_ENTRY_PATTERN);
        ints = initParams(Integer.class, intParams, EXTRA_INT_PARAM_ENTRY_PATTERN);
        longs = initParams(Long.class, longParams, EXTRA_LONG_PARAM_ENTRY_PATTERN);
        doubles = initParams(Double.class, doubleParams, EXTRA_DOUBLE_PARAM_ENTRY_PATTERN);
        strings = initParams(String.class, stringParams, EXTRA_STRING_PARAM_ENTRY_PATTERN);
    }

    public Map<String, Boolean> getBooleans() {
        return booleans;
    }

    public Map<String, Double> getDoubles() {
        return doubles;
    }

    public Map<String, Integer> getInts() {
        return ints;
    }

    public Map<String, Long> getLongs() {
        return longs;
    }

    public Map<String, Short> getShorts() {
        return shorts;
    }

    public Map<String, String> getStrings() {
        return strings;
    }

    protected <T> Map<String, T> initParams(Class<T> klass, String extraParams, Pattern pattern) {

        if (extraParams == null || extraParams.isEmpty()) {
            // not using
            return null;
        }

        StringTokenizer stk = new StringTokenizer(extraParams, "|");
        Map<String, T> result = new TreeMap<>();
        while (stk.hasMoreTokens()) {
            String entry = stk.nextToken();
            Matcher matcher = pattern.matcher(entry);
            if (!matcher.matches()) {
                throw new IllegalStateException("could not parse for extra params " + extraParams + " for type " + klass.getName());
            }
            String paramName = matcher.group(1);
            String paramValueStr = matcher.group(2);
            T paramValue = convert(klass, paramValueStr);
            if (log.isDebugEnabled()) {
                log.debug("detected extra param : <type:" + klass + ", name:" + paramName + ", value:" + paramValue + ">");
            }
            result.put(paramName, paramValue);
        }
        return result;
    }

    private <T> T convert(Class<T> klass, String paramValueStr) {
        if (Boolean.class.equals(klass)) {
            return klass.cast(Boolean.parseBoolean(paramValueStr));
        }
        if (Short.class.equals(klass)) {
            return klass.cast(Short.parseShort(paramValueStr));
        }
        if (Integer.class.equals(klass)) {
            return klass.cast(Integer.parseInt(paramValueStr));
        }
        if (Long.class.equals(klass)) {
            return klass.cast(Long.parseLong(paramValueStr));
        }
        if (Double.class.equals(klass)) {
            return klass.cast(Double.parseDouble(paramValueStr));
        }
        if (String.class.equals(klass)) {
            return klass.cast(paramValueStr);
        }
        throw new IllegalStateException("Could not convert type: " + klass.getName());
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends FieldExpressionValidator.Generator {
        public Generator() {
            super(FieldExpressionWithParamsValidator.class);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            StringBuilder parametersBuilder = new StringBuilder();
            validatorDefinition.getOptionalParameter("booleans").ifPresent(s -> parametersBuilder.append(String.format(", %s, %s", ExpressionResolver.escapeString("booleans"), ExpressionResolver.escapeString(s))));
            validatorDefinition.getOptionalParameter("shorts").ifPresent(s -> parametersBuilder.append(String.format(", %s, %s", ExpressionResolver.escapeString("shorts"), ExpressionResolver.escapeString(s))));
            validatorDefinition.getOptionalParameter("ints").ifPresent(s -> parametersBuilder.append(String.format(", %s, %s", ExpressionResolver.escapeString("ints"), ExpressionResolver.escapeString(s))));
            validatorDefinition.getOptionalParameter("longs").ifPresent(s -> parametersBuilder.append(String.format(", %s, %s", ExpressionResolver.escapeString("longs"), ExpressionResolver.escapeString(s))));
            validatorDefinition.getOptionalParameter("doubles").ifPresent(s -> parametersBuilder.append(String.format(", %s, %s", ExpressionResolver.escapeString("doubles"), ExpressionResolver.escapeString(s))));
            validatorDefinition.getOptionalParameter("strings").ifPresent(s -> parametersBuilder.append(String.format(", %s, %s", ExpressionResolver.escapeString("strings"), ExpressionResolver.escapeString(s))));
            String parameters = parametersBuilder.toString();
            importManager.addImport(Map.class);
            result.add(String.format("Map.of(%s)", parameters.isEmpty() ? "" : parameters.substring(2)));
        }
    }

}
