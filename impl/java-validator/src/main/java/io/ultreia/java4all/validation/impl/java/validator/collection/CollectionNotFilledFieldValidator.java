package io.ultreia.java4all.validation.impl.java.validator.collection;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.FieldValidatorSupport;

import java.util.Collection;
import java.util.function.Function;

/**
 * Created by tchemit on 19/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class CollectionNotFilledFieldValidator<O, F> extends FieldValidatorSupport<O, Collection<F>> {
    public CollectionNotFilledFieldValidator(String fieldName, Function<O, Collection<F>> fieldFunction) {
        super(fieldName, fieldFunction);
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        Collection<?> fieldValue = getField(object);
        if (fieldValue == null || fieldValue.isEmpty()) {
            addMessage(validationContext, messagesCollector, I18n.n("observe.Common.validation.field.not.filled"));
        }
    }

    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(CollectionNotFilledFieldValidator.class, true, true, false, false);
        }
    }

}
