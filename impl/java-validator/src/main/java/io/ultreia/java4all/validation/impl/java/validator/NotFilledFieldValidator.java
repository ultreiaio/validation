package io.ultreia.java4all.validation.impl.java.validator;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.auto.service.AutoService;
import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;

import java.util.function.Function;

/**
 * Created by tchemit on 19/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class NotFilledFieldValidator<O, F> extends FieldValidatorSupport<O, F> {
    public NotFilledFieldValidator(String fieldName,
                                   Function<O, F> fieldFunction,
                                   String messageKey,
                                   MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder) {
        super(fieldName, fieldFunction, messageKey == null ? I18n.n("observe.Common.validation.field.not.filled") : messageKey, messageBuilder);
    }

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        F value = getField(object);
        if (value == null) {
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
        }
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class Generator extends GeneratorSupport {
        public Generator() {
            super(NotFilledFieldValidator.class);
        }
    }

}
