package io.ultreia.java4all.validation.impl.java.validator.temporal;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import io.ultreia.java4all.validation.impl.java.validator.SkipableFieldValidatorSupport;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * To compare two dates (the one for the {@link #getFieldName()} against the {@link #otherDateFunction}.
 * <p>
 * <p>
 * If otherDateProperty value is {@code now}, then use current date.
 * <p>
 * If one of the two dates is null, then do not validate.
 * <p>
 * If validation failed, then can access the {@link #getStartDate()} and {@link #getEndDate()} in the message.
 * <p>
 * Created on 11/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.24
 */
public abstract class DateSupport<O> extends SkipableFieldValidatorSupport<O, Date> {

    private final BiFunction<O, NuitonValidationContext, Date> otherDateFunction;
    private final String datePattern;
    private final boolean strict;
    private final Integer maxDelayInMinutes;
    private final Integer minDelayInMinutes;
    private final Integer maxDelayInDays;
    private final Integer minDelayInDays;
    private final String delayErrorMessage;
    private Date endDate;
    private Date startDate;

    public DateSupport(String fieldName, Function<O, Date> fieldFunction, String messageKey, MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder, BiFunction<O, NuitonValidationContext, Boolean> skipFunction, boolean strict, String datePattern, BiFunction<O, NuitonValidationContext, Date> otherDateFunction, Map<String, Object> parameters) {
        super(fieldName, fieldFunction, messageKey, messageBuilder, skipFunction);
        this.otherDateFunction = otherDateFunction;
        this.datePattern = datePattern;
        this.delayErrorMessage = (String) parameters.get("delayErrorMessage");
        this.maxDelayInMinutes = (Integer) parameters.get("maxDelayInMinutes");
        this.minDelayInMinutes = (Integer) parameters.get("minDelayInMinutes");
        this.maxDelayInDays = (Integer) parameters.get("maxDelayInDays");
        this.minDelayInDays = (Integer) parameters.get("minDelayInDays");
        this.strict = strict;
    }

    @Override
    protected void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {
        startDate = getStartDate(object, validationContext);
        if (startDate == null) {
            return;
        }
        endDate = getEndDate(object, validationContext);
        if (endDate == null) {
            return;
        }

        boolean valid = isValid(startDate, endDate);
        if (!valid) {
            // basic error
            addMessage(validationContext, messagesCollector, getMessage(object, validationContext));
            return;
        }
        boolean usingMinutes = maxDelayInMinutes != null || minDelayInMinutes != null;
        boolean usingDays = maxDelayInDays != null || minDelayInDays != null;
        if (usingMinutes) {
            long delayInMinutes = TimeUnit.MILLISECONDS.toMinutes(endDate.getTime() - startDate.getTime());
            validateDelay(object, validationContext, messagesCollector, delayInMinutes, minDelayInMinutes, maxDelayInMinutes);
            return;
        }
        if (usingDays) {
            long delayInDays = TimeUnit.MILLISECONDS.toDays(endDate.getTime() - startDate.getTime());
            validateDelay(object, validationContext, messagesCollector, delayInDays, minDelayInDays, maxDelayInDays);
        }
    }

    protected abstract Date getStartDate(O object, NuitonValidationContext validationContext);

    protected abstract Date getEndDate(O object, NuitonValidationContext validationContext);

    public String getStartDate() {
        return String.format(datePattern, startDate);
    }

    public String getEndDate() {
        return String.format(datePattern, endDate);
    }

    protected boolean isValid(Date beginDate, Date endDate) {
        long beginDateTime = beginDate.getTime();
        long endDateTime = endDate.getTime();
        if (beginDateTime < endDateTime) {
            return true;
        }
        return !strict && beginDateTime == endDateTime;
    }

    protected Date format(Date date) {
        return date;
    }

    protected Date getThisDate(O object, NuitonValidationContext validationContext) {
        Date date = getField(object);
        return date == null ? null : format(date);
    }

    protected Date getAgainstDate(O object, NuitonValidationContext validationContext) {
        Date date = otherDateFunction.apply(object, validationContext);
        return date == null ? null : format(date);
    }

    private void validateDelay(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, long delay, Integer min, Integer max) {
        if (min != null) {
            boolean valid = delay >= min;
            if (!valid) {
                addMessage(validationContext, messagesCollector, delayErrorMessage, delay, min);
            }
        }
        if (max != null) {
            boolean valid = delay <= max;
            if (!valid) {
                addMessage(validationContext, messagesCollector, delayErrorMessage, delay, max);
            }
        }
    }


    @AutoService(FieldValidatorGenerator.class)
    public static class DateSupportGenerator extends GeneratorSupport {
        public DateSupportGenerator() {
            super(DateSupport.class);
        }

        @Override
        public boolean accept(Class<?> validationType) {
            return DateSupport.class.isAssignableFrom(validationType);
        }

        @Override
        protected void generateExtraParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager, List<String> result) throws NoSuchMethodException {
            super.generateExtraParameters(key, validatorDefinition, validationContextType, importManager, result);
            result.add(validatorDefinition.getOptionalParameter("strict").orElse("false"));
            addOtherDateFunction(key, validatorDefinition, validationContextType, result);
            addParameters(validatorDefinition, importManager, result);
        }

        protected void addOtherDateFunction(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, List<String> result) throws NoSuchMethodException {
            String pattern = validatorDefinition.getParameter("otherDateProperty").trim();
            ExpressionResolver resolver = beanAndValidationContextResolver(key.getBeanType(), validationContextType);
            result.add("(o, c) -> " + resolver.resolve(pattern));
        }

        protected void addParameters(FieldValidatorDefinition validatorDefinition, ImportManager importManager, List<String> result) {
            importManager.addImport(Map.class);
            StringBuilder parametersBuilder = new StringBuilder();
            validatorDefinition.getOptionalParameter("maxDelayInMinutes").ifPresent(v -> parametersBuilder.append(String.format("%s, %s,", ExpressionResolver.escapeString("maxDelayInMinutes"), v)));
            validatorDefinition.getOptionalParameter("minDelayInMinutes").ifPresent(v -> parametersBuilder.append(String.format("%s, %s,", ExpressionResolver.escapeString("minDelayInMinutes"), v)));
            validatorDefinition.getOptionalParameter("maxDelayInDays").ifPresent(v -> parametersBuilder.append(String.format("%s, %s,", ExpressionResolver.escapeString("maxDelayInDays"), v)));
            validatorDefinition.getOptionalParameter("minDelayInDays").ifPresent(v -> parametersBuilder.append(String.format("%s, %s,", ExpressionResolver.escapeString("minDelayInDays"), v)));
            validatorDefinition.getOptionalParameter("delayErrorMessage").ifPresent(v -> parametersBuilder.append(String.format("%s, I18n.n(%s),", ExpressionResolver.escapeString("delayErrorMessage"), ExpressionResolver.escapeString(v))));

            result.add(String.format("Map.of(%s)", parametersBuilder.length() == 0 ? "" : parametersBuilder.substring(0, parametersBuilder.length() - 1)));
        }
    }
}
