package io.ultreia.java4all.validation.impl.java.validator.temporal;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;

import java.util.Date;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * TTo check if this date is before against property. If not fail.
 * <p>
 * Created on 11/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 5.0.24
 */
class DateBeforeSupport<O> extends DateSupport<O> {
    public DateBeforeSupport(String fieldName,
                             Function<O, Date> fieldFunction,
                             String messageKey,
                             MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder,
                             BiFunction<O, NuitonValidationContext, Boolean> skipFunction,
                             boolean strict,
                             String datePattern,
                             BiFunction<O, NuitonValidationContext, Date> otherDateFunction,
                             Map<String, Object> parameters) {
        super(fieldName, fieldFunction, messageKey, messageBuilder, skipFunction, strict, datePattern, otherDateFunction, parameters);
    }

    @Override
    protected Date getStartDate(O object, NuitonValidationContext validationContext) {
        return getThisDate(object, validationContext);
    }

    @Override
    protected Date getEndDate(O object, NuitonValidationContext validationContext) {
        return getAgainstDate(object, validationContext);
    }
}
