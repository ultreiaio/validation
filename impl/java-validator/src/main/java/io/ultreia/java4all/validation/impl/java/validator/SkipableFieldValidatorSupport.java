package io.ultreia.java4all.validation.impl.java.validator;

/*-
 * #%L
 * Validation :: Impl :: Java Validator
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.MessageBuilder;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * This validator offers a way to skip validation.
 * * <p>
 * * To use this new field validator support, just now implements the method
 * * {@link #validateWhenNotSkip(Object, NuitonValidationContext, ValidationMessagesCollector)}. This method will be invoked only if the skip
 * * parameter is evaluated to {@code false}.
 * Created on 28/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public abstract class SkipableFieldValidatorSupport<O, F> extends FieldValidatorSupport<O, F> {

    private static final Logger log = LogManager.getLogger(SkipableFieldValidatorSupport.class);

    private final BiFunction<O, NuitonValidationContext, Boolean> skipFunction;

    public SkipableFieldValidatorSupport(String fieldName, Function<O, F> fieldFunction) {
        this(fieldName, fieldFunction, null, null, null);
    }

    public SkipableFieldValidatorSupport(String fieldName, Function<O, F> fieldFunction, BiFunction<O, NuitonValidationContext, Boolean> skipFunction) {
        this(fieldName, fieldFunction, null, null, skipFunction);
    }

    public SkipableFieldValidatorSupport(String fieldName, Function<O, F> fieldFunction, String messageKey, MessageBuilder<O, ? super NuitonValidationContext, ? super FieldValidator<O, ?>> messageBuilder, BiFunction<O, NuitonValidationContext, Boolean> skipFunction) {
        super(fieldName, fieldFunction, messageKey, messageBuilder);
        this.skipFunction = skipFunction;
    }

    /**
     * Method to be invoked when skip parameter was not evaluated to {@code true}.
     *
     * @param object            the object to be validated.
     * @param validationContext to get access to outside world
     * @param messagesCollector to collect messages
     */
    protected abstract void validateWhenNotSkip(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector);

    @Override
    public void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {

        // evaluate the skip parameter
        boolean mustSkip = evaluateSkipParameter(object, validationContext);

        if (mustSkip) {

            // skip is set to true, so skip the validation
            if (log.isDebugEnabled()) {
                log.debug("Skip the validation from " + this + ", due to skip parameter evaluated to true");
            }
            return;
        }

        // must validate
        validateWhenNotSkip(object, validationContext, messagesCollector);
    }

    /**
     * Evaluate the skip parameter value against the object to validate.
     * <p>
     * This parameter can be an OGNL expression.
     *
     * @param object            the object to validate
     * @param validationContext validation context
     * @return the evaluation of the skip parameter.
     */
    protected boolean evaluateSkipParameter(O object, NuitonValidationContext validationContext) {
        return skipFunction != null && skipFunction.apply(object, validationContext);
    }

    protected BiFunction<O, NuitonValidationContext, Boolean> skipFunction() {
        return skipFunction;
    }

    public static abstract class GeneratorSupport extends FieldValidatorSupport.GeneratorSupport {

        private final boolean addSkipFunction;

        public GeneratorSupport(Class<?> validatorType, boolean addFieldName, boolean addFieldFunction, boolean addMessageKey, boolean addMessageBuilder, boolean addSkipFunction) {
            super(validatorType, addFieldName, addFieldFunction, addMessageKey, addMessageBuilder);
            this.addSkipFunction = addSkipFunction;
        }

        public GeneratorSupport(Class<?> validatorType) {
            this(validatorType, true, true, true, true, true);
        }

        protected List<String> generateParameters(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, ImportManager importManager) throws NoSuchMethodException {
            List<String> result = super.generateParameters(key, validatorDefinition, validationContextType, importManager);
            if (addSkipFunction) {
                // add skip function
                addSkipFunctionParameter(key, validatorDefinition, validationContextType, result);
            }
            return result;
        }

        private void addSkipFunctionParameter(FileValidatorEntryDefinition key, FieldValidatorDefinition validatorDefinition, Class<? extends NuitonValidationContext> validationContextType, List<String> result) throws NoSuchMethodException {
            Optional<String> optionalSkip = validatorDefinition.getOptionalParameter("skip");
            if (optionalSkip.isEmpty()) {
                // no skip parameter
                result.add("null");
                return;
            }
            String skip = optionalSkip.get().trim();
            if (skip.equals("true") || skip.equals("false")) {
                result.add("(o, c) -> " + skip);
                return;
            }
            ExpressionResolver resolver = beanAndValidationContextResolver(key.getBeanType(), validationContextType);
            result.add("(o, c) -> " + resolver.resolve(skip));
        }
    }

}
