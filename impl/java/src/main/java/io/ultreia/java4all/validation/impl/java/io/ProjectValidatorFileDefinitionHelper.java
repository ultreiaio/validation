package io.ultreia.java4all.validation.impl.java.io;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.util.json.adapters.ClassAdapter;
import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorFileDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created on 27/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class ProjectValidatorFileDefinitionHelper {

    public static final String LOCATION = "META-INF/validation/files.json";
    private static final Logger log = LogManager.getLogger(ProjectValidatorFileDefinitionHelper.class);

    static Gson creatGson() {
        return new GsonBuilder().setPrettyPrinting().registerTypeAdapter(Class.class, new ClassAdapter()).disableHtmlEscaping().create();
    }

    public ProjectValidatorFileDefinition build(Path rootPath) throws IOException {
        List<String> pathsByEntry = detectFiles(rootPath);
        return new ProjectValidatorFileDefinition(pathsByEntry);
    }

    public ProjectValidatorFileDefinition readAll() throws IOException {
        Enumeration<URL> resource = Objects.requireNonNull(ProjectValidatorFileDefinition.class.getClassLoader().getResources(LOCATION));
        Gson gson = creatGson();
        List<String> filesByEntry = new LinkedList<>();
        while (resource.hasMoreElements()) {
            URL url = resource.nextElement();
            filesByEntry.addAll(readFile(gson, url));
        }
        filesByEntry.sort(String::compareTo);
        return new ProjectValidatorFileDefinition(filesByEntry);
    }

    public ProjectValidatorFileDefinition read(URL url) throws IOException {
        Gson gson = creatGson();
        List<String> filesByEntry = readFile(gson, url);
        filesByEntry.sort(String::compareTo);
        return new ProjectValidatorFileDefinition(filesByEntry);
    }

    public ProjectValidatorFileDefinition read(Path path) throws IOException {
        return read(toLocation(path).toUri().toURL());
    }

    public Path write(ProjectValidatorFileDefinition model, Path target) throws IOException {
        Gson gson = creatGson();
        String content = gson.toJson(model.getFiles());
        Path location = toLocation(target);
        if (Files.notExists(location.getParent())) {
            Files.createDirectories(location.getParent());
        }
        Files.writeString(location, content);
        return location;
    }

    public Path toLocation(Path target) {
        return target.resolve(LOCATION);
    }

    private List<String> detectFiles(Path rootPath) throws IOException {
        log.info("Scan rootPath {}", rootPath);
        List<String> result = new LinkedList<>();
        Files.walkFileTree(rootPath, new FileVisitor<>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                if (file.toFile().getName().endsWith("-validation.json")) {
                    log.trace("Scan file {}", file);
                    String path = "/" + rootPath.relativize(file);
                    result.add(path);
                    log.debug("Detect validation file: {}", path);
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
                return FileVisitResult.CONTINUE;
            }
        });
        result.sort(String::compareTo);
        log.info("{} file(s) detected.", result.size());
        return result;
    }

    private List<String> readFile(Gson gson, URL url) throws IOException {
        log.info("Loading file: {}", url);
        try (Reader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            return gson.fromJson(reader, new TypeToken<List<String>>() {
            }.getType());
        }
    }
}
