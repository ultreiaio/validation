package io.ultreia.java4all.validation.impl.java;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidatorFileInfo;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.io.FileValidatorModelBuilder;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created at 09/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonValidatorFileInfoImpl extends NuitonValidatorFileInfo {

    private transient FileValidatorDefinition model;

    public NuitonValidatorFileInfoImpl(Path file, Class<?> type, String context, NuitonValidatorScope scope, Set<String> fields) {
        super(file, type, context, scope, fields);
    }

    @Override
    protected Map<String, List<String>> createComments() {
        Map<String, List<String>> comments = new TreeMap<>();
        FileValidatorDefinition model = getModel();
        model.getFields().forEach((fieldName, validators) -> validators.forEach(v -> comments.computeIfAbsent(fieldName, k -> new LinkedList<>()).add(v.getComment())));
        return comments;
    }

    protected FileValidatorDefinition getModel() {
        if (model == null) {
            try {
                model = new FileValidatorModelBuilder().build(getFile().toUri().toURL(), getType(), getScope().name(), getContext());
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }
        return model;
    }
}
