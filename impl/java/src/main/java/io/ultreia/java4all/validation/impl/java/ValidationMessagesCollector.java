package io.ultreia.java4all.validation.impl.java;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created at 31/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class ValidationMessagesCollector {

    private final Map<String, List<String>> fieldMessages;

    public ValidationMessagesCollector() {
        fieldMessages = new LinkedHashMap<>();
    }

    public void addMessage(String fieldName, String message) {
        fieldMessages.computeIfAbsent(fieldName, k -> new LinkedList<>()).add(message);
    }

    public Map<String, List<String>> getFieldMessages() {
        return Collections.unmodifiableMap(new LinkedHashMap<>(fieldMessages));
    }

    public boolean isNotEmpty() {
        return !fieldMessages.isEmpty();
    }

    public void clear() {
        fieldMessages.clear();
    }
}
