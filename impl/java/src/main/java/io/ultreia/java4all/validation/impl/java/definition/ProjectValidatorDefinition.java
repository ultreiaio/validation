package io.ultreia.java4all.validation.impl.java.definition;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonFieldValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorModelEntry;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Describes the project validators file.
 * <p>
 * Created on 27/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class ProjectValidatorDefinition {

    private final Map<FileValidatorEntryDefinition, FileValidatorDefinition> filesByEntry;

    public ProjectValidatorDefinition(Map<FileValidatorEntryDefinition, FileValidatorDefinition> filesByEntry) {
        this.filesByEntry = Collections.unmodifiableMap(Objects.requireNonNull(filesByEntry));
    }

    public Map<FileValidatorEntryDefinition, FileValidatorDefinition> getFilesByEntry() {
        return filesByEntry;
    }

    public Stream<Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition>> filterByType(Class<?> type) {
        return getFilesByEntry()
                .entrySet()
                .stream()
                .filter(e -> e.getKey().getBeanType().isAssignableFrom(type));
    }

    public List<NuitonValidatorModel<?>> toModels() {
        List<NuitonValidatorModel<?>> models = new LinkedList<>();
        Set<Class<?>> beanTypes = getBeanTypes();
        for (Class<?> beanType : beanTypes) {
            LinkedHashSet<Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition>> beanUniverse = getFilesByEntry().entrySet().stream().filter(f -> beanType.equals(f.getKey().getBeanType())).collect(Collectors.toCollection(LinkedHashSet::new));
            Set<String> contexts = getBeanContexts(beanUniverse);
            for (String context : contexts) {
                LinkedHashSet<Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition>> contextUniverse = beanUniverse.stream().filter(f -> Objects.equals(context, f.getKey().getContext())).collect(Collectors.toCollection(LinkedHashSet::new));
                Set<NuitonValidatorScope> scopes = getBeanScopes(contextUniverse);
                NuitonValidatorModelEntry<?> entry = new NuitonValidatorModelEntry<>(beanType, context, scopes);
                NuitonValidatorModel<?> model = toModel(contextUniverse, entry);
                models.add(model);
            }
        }
        return models;
    }

    protected <O> NuitonValidatorModel<O> toModel(LinkedHashSet<Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition>> contextUniverse, NuitonValidatorModelEntry<O> entry) {
        Map<NuitonValidatorScope, List<NuitonFieldValidatorModel>> fields = new TreeMap<>(Comparator.comparing(NuitonValidatorScope::ordinal));
        contextUniverse
                .stream()
                .map(Map.Entry::getValue)
                .forEach(d -> {
                    List<NuitonFieldValidatorModel> fieldModels = fields.computeIfAbsent(NuitonValidatorScope.valueOf(d.getScope().toUpperCase()), e -> new LinkedList<>());
                    fieldModels.addAll(d.toModel());
                });
        return new NuitonValidatorModel<>(entry, fields);
    }

    protected Set<Class<?>> getBeanTypes() {
        return getFilesByEntry().keySet().stream().map(FileValidatorEntryDefinition::getBeanType).sorted(Comparator.comparing(Class::getName)).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    protected Set<String> getBeanContexts(LinkedHashSet<Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition>> beanUniverse) {
        return beanUniverse.stream().map(Map.Entry::getKey).map(FileValidatorEntryDefinition::getContext).sorted(Comparator.nullsFirst(String::compareTo)).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    protected Set<NuitonValidatorScope> getBeanScopes(LinkedHashSet<Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition>> contextUniverse) {
        return contextUniverse.stream().map(Map.Entry::getKey).map(FileValidatorEntryDefinition::getScope).map(String::toUpperCase).map(NuitonValidatorScope::valueOf).sorted().collect(Collectors.toCollection(LinkedHashSet::new));
    }

}
