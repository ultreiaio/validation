package io.ultreia.java4all.validation.impl.java.io;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static io.ultreia.java4all.validation.impl.java.io.ProjectValidatorFileDefinitionHelper.creatGson;

/**
 * To build a {@link FileValidatorDefinition}.
 * <p>
 * Created on 26/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class FileValidatorModelBuilder {
    private static final Logger log = LogManager.getLogger(FileValidatorModelBuilder.class);
    private final Gson gson;

    public FileValidatorModelBuilder() {
        gson = creatGson();
    }

    public FileValidatorDefinition build(URL file, Class<?> type, String scope, String context) {
        log.info("Building fileValidatorModel for type {} - scope {} - context {}", type.getName(), scope, context);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.openStream()))) {
            Map<String, List<FieldValidatorDefinition>> fields = gson.fromJson(reader, new TypeToken<Map<String, List<FieldValidatorDefinition>>>() {
            }.getType());
            Map<String, List<FieldValidatorDefinition>> finalFields = new TreeMap<>();
            fields.forEach((k, v) -> {
                List<FieldValidatorDefinition> finalFieldValidatorDefinitions = new ArrayList<>();
                finalFields.put(k, finalFieldValidatorDefinitions);
                v.forEach(f -> {
                    FieldValidatorDefinition newField = new FieldValidatorDefinition(k, f.getComment(), f.getValidator(), f.getParameters(), f.getMessage());
                    finalFieldValidatorDefinitions.add(newField);
                });
            });
            finalFields.forEach((k, v) -> v.sort(Comparator.comparing(FieldValidatorDefinition::getFieldName)));
            return new FileValidatorDefinition(type, scope, context, finalFields);
        } catch (Exception e) {
            throw new RuntimeException("Could not load file " + file, e);
        }
    }
}
