package io.ultreia.java4all.validation.impl.java.definition;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Created on 27/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class FileValidatorEntryDefinition implements Comparable<FileValidatorEntryDefinition> {
    private final Class<?> beanType;
    private final String context;
    private final String scope;
    private final String key;

    public FileValidatorEntryDefinition(Class<?> beanType, String context, String scope) {
        this.beanType = Objects.requireNonNull(beanType);
        this.context = context;
        this.scope = Objects.requireNonNull(scope);
        this.key = String.format("%s-%s-%s", beanType.getName(), context, scope);
    }

    public Class<?> getBeanType() {
        return beanType;
    }

    public String getScope() {
        return scope;
    }

    public String getContext() {
        return context;
    }

    public String getKey() {
        return key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileValidatorEntryDefinition)) return false;
        FileValidatorEntryDefinition fileValidatorEntryDefinition = (FileValidatorEntryDefinition) o;
        return Objects.equals(key, fileValidatorEntryDefinition.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", FileValidatorEntryDefinition.class.getSimpleName() + "[", "]")
                .add("key='" + key + "'")
                .toString();
    }

    @Override
    public int compareTo(FileValidatorEntryDefinition o) {
        return key.compareTo(o.getKey());
    }

}
