package io.ultreia.java4all.validation.impl.java;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonScopeValidator;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;

import java.util.Collections;
import java.util.Map;

/**
 * Contains the mapping from validator entry to his generated class.
 * <p>
 * Created at 02/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class ProjectValidatorMapping {

    private final Map<String, Class<? extends NuitonScopeValidator<?>>> mapping;

    public ProjectValidatorMapping(Map<String, Class<? extends NuitonScopeValidator<?>>> mapping) {
        this.mapping = Collections.unmodifiableMap(mapping);
    }

    @SuppressWarnings("unchecked")
    public <V extends NuitonScopeValidator<?>> Class<V> getMapping(Class<?> beanType, String context, String scope) {
        String key = String.format("%s-%s-%s", beanType.getName(), context, scope);
        return (Class<V>) mapping.get(key);
    }

    public Class<? extends NuitonScopeValidator<?>> getMapping(FileValidatorEntryDefinition entry) {
        return mapping.get(entry.getKey());
    }

    public Map<String, Class<? extends NuitonScopeValidator<?>>> getMapping() {
        return mapping;
    }
}
