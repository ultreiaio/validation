package io.ultreia.java4all.validation.impl.java.io;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.impl.java.definition.FileValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorFileDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * To build a {@link ProjectValidatorDefinition}.
 * <p>
 * Created on 27/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class ProjectValidatorDefinitionBuilder {
    private static final Logger log = LogManager.getLogger(ProjectValidatorDefinitionBuilder.class);
    private final FileValidatorModelBuilder fileValidatorModelBuilder;
    private final ClassLoader classLoader;

    public ProjectValidatorDefinitionBuilder() {
        this(null);
    }

    public ProjectValidatorDefinitionBuilder(ClassLoader classLoader) {
        this.classLoader = classLoader == null ? getClass().getClassLoader() : classLoader;
        fileValidatorModelBuilder = new FileValidatorModelBuilder();
    }

    public static FileValidatorEntryDefinition toEntryDefinition(String location) {
        return toEntryDefinition(ProjectValidatorDefinitionBuilder.class.getClassLoader(), location);
    }

    public static FileValidatorEntryDefinition toEntryDefinition(ClassLoader classLoader, String location) {
        Path path = Path.of(Objects.requireNonNull(location));
        Path packagePath = path.getParent();
        String packageName = packagePath.toString().substring(1).replaceAll("/", ".");
        String name = path.toFile().getName();
        int index = name.indexOf('-');
        String classSimpleName = name.substring(0, index);
        String className = packageName + "." + classSimpleName;
        Class<?> classType;
        try {
            classType = classLoader.loadClass(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        String contextAndScope = name.substring(index + 1, name.lastIndexOf('-'));
        index = contextAndScope.lastIndexOf('-');
        String scope = contextAndScope.substring(index + 1);
        String context = index == -1 ? null : contextAndScope.substring(0, index);
        return new FileValidatorEntryDefinition(classType, context, scope);
    }

    public ProjectValidatorDefinition buildAll() throws IOException {
        ProjectValidatorFileDefinition filesModel = new ProjectValidatorFileDefinitionHelper().readAll();
        return createProjectValidatorModel(filesModel);
    }

    public ProjectValidatorDefinition build(URL location) throws IOException {
        ProjectValidatorFileDefinition filesModel = new ProjectValidatorFileDefinitionHelper().read(location);
        return createProjectValidatorModel(filesModel);
    }

    private ProjectValidatorDefinition createProjectValidatorModel(ProjectValidatorFileDefinition filesModel) {
        Map<FileValidatorEntryDefinition, FileValidatorDefinition> filesByEntry = new TreeMap<>(Comparator.comparing(FileValidatorEntryDefinition::getKey));
        for (String file : filesModel.getFiles()) {
            FileValidatorEntryDefinition key = ProjectValidatorDefinitionBuilder.toEntryDefinition(classLoader, file);
            URL resource = Objects.requireNonNull(classLoader.getResource(file.substring(1)));
            log.info("Loading {} → {}", resource, key);
            FileValidatorDefinition fileValidatorDefinition = fileValidatorModelBuilder.build(resource, key.getBeanType(), key.getScope(), key.getContext());
            filesByEntry.put(key, fileValidatorDefinition);
        }
        return new ProjectValidatorDefinition(filesByEntry);
    }
}
