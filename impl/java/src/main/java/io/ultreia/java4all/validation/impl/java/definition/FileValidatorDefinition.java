package io.ultreia.java4all.validation.impl.java.definition;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonFieldValidatorModel;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Describes a hole object validation declaration file definition.
 * <p>
 * Created on 26/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class FileValidatorDefinition {

    private final Class<?> beanType;
    private final String scope;
    private final String context;
    private final Map<String, List<FieldValidatorDefinition>> fields;

    public FileValidatorDefinition(Class<?> beanType, String scope, String context, Map<String, List<FieldValidatorDefinition>> fields) {
        this.beanType = Objects.requireNonNull(beanType);
        this.scope = Objects.requireNonNull(scope);
        this.context = context;
        this.fields = Objects.requireNonNull(fields);
    }

    public Class<?> getBeanType() {
        return beanType;
    }

    public String getScope() {
        return scope;
    }

    public String getContext() {
        return context;
    }

    public Map<String, List<FieldValidatorDefinition>> getFields() {
        return fields;
    }

    public List<NuitonFieldValidatorModel> toModel() {
        List<NuitonFieldValidatorModel> result = new LinkedList<>();
        fields.forEach((fieldName, fields) -> {
            Set<String> comments = fields.stream().map(FieldValidatorDefinition::getComment).collect(Collectors.toCollection(LinkedHashSet::new));
            result.add(new NuitonFieldValidatorModel(fieldName, comments));
        });
        result.sort(Comparator.comparing(NuitonFieldValidatorModel::getFieldName));
        return result;
    }
}
