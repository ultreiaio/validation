package io.ultreia.java4all.validation.impl.java.io;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.util.json.adapters.ClassAdapter;
import io.ultreia.java4all.validation.api.NuitonScopeValidator;
import io.ultreia.java4all.validation.impl.java.ProjectValidatorMapping;
import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorFileDefinition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Created at 02/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class ProjectValidatorMappingHelper {

    public static final String LOCATION = "META-INF/validation/mapping.json";
    private static final Logger log = LogManager.getLogger(ProjectValidatorMappingHelper.class);

    static Gson creatGson() {
        return new GsonBuilder().setPrettyPrinting().registerTypeAdapter(Class.class, new ClassAdapter()).create();
    }

    public ProjectValidatorMapping readAll() throws IOException {
        Enumeration<URL> resource = Objects.requireNonNull(ProjectValidatorFileDefinition.class.getClassLoader().getResources(LOCATION));
        Gson gson = creatGson();
        Map<String, Class<? extends NuitonScopeValidator<?>>> mapping = new TreeMap<>();
        while (resource.hasMoreElements()) {
            URL url = resource.nextElement();
            mapping.putAll(readFile(gson, url));
        }
        return new ProjectValidatorMapping(mapping);
    }

    public ProjectValidatorMapping read(URL url) throws IOException {
        Gson gson = creatGson();
        Map<String, Class<? extends NuitonScopeValidator<?>>> filesByEntry = readFile(gson, url);
        return new ProjectValidatorMapping(filesByEntry);
    }

    public ProjectValidatorMapping read(Path path) throws IOException {
        return read(toLocation(path).toUri().toURL());
    }

    public Path write(ProjectValidatorMapping model, Path target) throws IOException {
        Gson gson = creatGson();
        String content = gson.toJson(model.getMapping());
        Path location = toLocation(target);
        if (Files.notExists(location.getParent())) {
            Files.createDirectories(location.getParent());
        }
        Files.writeString(location, content);
        return location;
    }

    public Path write(Map<String, String> model, Path target) throws IOException {
        Gson gson = creatGson();
        String content = gson.toJson(model);
        Path location = toLocation(target);
        if (Files.notExists(location.getParent())) {
            Files.createDirectories(location.getParent());
        }
        Files.writeString(location, content);
        return location;
    }

    public Path toLocation(Path target) {
        return target.resolve(LOCATION);
    }


    private Map<String, Class<? extends NuitonScopeValidator<?>>> readFile(Gson gson, URL url) throws IOException {
        log.info("Loading file: {}", url);
        try (Reader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            return gson.fromJson(reader, new TypeToken<Map<String, Class<? extends NuitonScopeValidator<?>>>>() {
            }.getType());
        }
    }
}

