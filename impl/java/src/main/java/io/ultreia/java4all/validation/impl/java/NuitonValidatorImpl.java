package io.ultreia.java4all.validation.impl.java;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Objects2;
import io.ultreia.java4all.validation.api.AbstractNuitonValidator;
import io.ultreia.java4all.validation.api.NuitonScopeValidator;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;

import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Created at 01/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonValidatorImpl<O> extends AbstractNuitonValidator<O> {

    private final Supplier<ProjectValidatorMapping> mappingSupplier;

    public NuitonValidatorImpl(Supplier<ProjectValidatorMapping> mappingSupplier, NuitonValidatorModel<O> model) {
        super(model);
        this.mappingSupplier = Objects.requireNonNull(mappingSupplier);
    }

    @Override
    protected NuitonScopeValidator<O> createScopeValidator(String context, NuitonValidatorScope scope, Class<O> type, Set<String> fields) {
        Class<NuitonScopeValidator<O>> aClass = mappingSupplier.get().getMapping(type, context, scope.name().toLowerCase());
        Objects.requireNonNull(aClass, String.format("Can't find validator class name for entry %s-%s-%s", type.getName(), context, scope));
        return Objects2.newInstance(aClass);
    }
}
