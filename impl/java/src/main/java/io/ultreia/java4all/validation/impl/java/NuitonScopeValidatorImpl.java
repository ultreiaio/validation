package io.ultreia.java4all.validation.impl.java;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.AbstractNuitonScopeValidator;
import io.ultreia.java4all.validation.api.NuitonValidationContext;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

/**
 * Created at 01/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public abstract class NuitonScopeValidatorImpl<O> extends AbstractNuitonScopeValidator<O> {
    private final FieldValidator<O, ?>[] validators;

    private String validatorComment;

    @SuppressWarnings("unchecked")
    public NuitonScopeValidatorImpl(Class<O> type, String context, Set<String> fieldNames, int validatorsCount) {
        super(Objects.requireNonNull(type), context, Objects.requireNonNull(fieldNames));
        this.validators = new FieldValidator[validatorsCount];
    }

    @Override
    public Map<String, List<String>> validate(O bean, NuitonValidationContext validationContext) {
        ValidationMessagesCollector messagesCollector = new ValidationMessagesCollector();
        validate(bean, validationContext, messagesCollector);
        return messagesCollector.getFieldMessages();
    }

    protected abstract void validate(O bean, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector);

    protected FieldValidator<O, ?> validator(int index, Supplier<FieldValidator<O, ?>> create) {
        FieldValidator<O, ?> validator = validators[index];
        if (validator == null) {
            validators[index] = validator = create.get();
        }
        return validator;
    }

    public String getValidatorComment() {
        return validatorComment;
    }

    public void setValidatorComment(String validatorComment) {
        this.validatorComment = validatorComment;
    }
}
