package io.ultreia.java4all.validation.impl.java.definition;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.impl.java.FieldValidator;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Describe a field validator declaration in a validation definition file.
 * <p>
 * Created on 26/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class FieldValidatorDefinition {

    private final String fieldName;
    private final String comment;
    private final Class<? extends FieldValidator<?, ?>> validator;
    private final Map<String, String> parameters;
    private final String message;

    public FieldValidatorDefinition(String fieldName, String comment, Class<? extends FieldValidator<?, ?>> validator, Map<String, String> parameters, String message) {
        this.fieldName = Objects.requireNonNull(fieldName);
        this.comment = Objects.requireNonNull(comment);
        this.validator = Objects.requireNonNull(validator);
        this.parameters = parameters;
        this.message = message;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getComment() {
        return comment;
    }

    public Class<? extends FieldValidator<?, ?>> getValidator() {
        return validator;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getMessage() {
        return message;
    }

    public boolean withoutParameters() {
        return parameters == null || parameters.isEmpty();
    }

    public boolean withoutMessage() {
        return message == null || message.isEmpty();
    }

    public String getParameter(String parameterName) {
        return parameters == null ? null : parameters.get(parameterName);
    }

    public Optional<String> getOptionalParameter(String parameterName) {
        return Optional.ofNullable(parameters == null ? null : parameters.get(parameterName));
    }

    public boolean withoutParameter(String parameterName) {
        return parameters == null || parameters.get(parameterName) == null;
    }
}
