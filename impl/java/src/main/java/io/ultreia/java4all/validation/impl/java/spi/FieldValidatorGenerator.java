package io.ultreia.java4all.validation.impl.java.spi;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;

/**
 * To generate the java code associated to a field validation type, using service loaders and method
 * {@link #accept(Class)} to get the code generator.
 * <p>
 * Created at 01/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public interface FieldValidatorGenerator {

    /**
     * @param validationType type of validator
     * @return {@code true} if this generator can be used for the given type.
     */
    boolean accept(Class<?> validationType);

    /**
     * @param key                   validation file key
     * @param validatorDefinition   definition of validator to generate
     * @param validationContextType validation context type
     * @param importManager         import manager used
     * @return the java code to instantiate the validator.
     * @throws NoSuchMethodException if some methods could not be found while generating java code.
     */
    String generate(FileValidatorEntryDefinition key,
                    FieldValidatorDefinition validatorDefinition,
                    Class<? extends NuitonValidationContext> validationContextType,
                    ImportManager importManager) throws NoSuchMethodException;

}
