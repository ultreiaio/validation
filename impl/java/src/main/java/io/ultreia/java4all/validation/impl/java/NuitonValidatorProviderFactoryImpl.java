package io.ultreia.java4all.validation.impl.java;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.validation.api.AbstractNuitonValidatorProviderFactory;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.api.NuitonValidator;
import io.ultreia.java4all.validation.api.NuitonValidatorFileInfo;
import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.NuitonValidatorModelEntry;
import io.ultreia.java4all.validation.api.NuitonValidatorModelProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorProvider;
import io.ultreia.java4all.validation.api.NuitonValidatorProviderFactory;
import io.ultreia.java4all.validation.api.NuitonValidatorScope;
import io.ultreia.java4all.validation.impl.java.io.ProjectValidatorMappingHelper;

import java.nio.file.Path;
import java.util.Set;

/**
 * Created at 09/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
@AutoService(NuitonValidatorProviderFactory.class)
public class NuitonValidatorProviderFactoryImpl extends AbstractNuitonValidatorProviderFactory {
    public static final String PROVIDER_NAME = "default";
    private ProjectValidatorMapping mapping;

    public NuitonValidatorProviderFactoryImpl() {
        super(PROVIDER_NAME);
    }

    @Override
    public NuitonValidatorProvider newProvider(NuitonValidationContext validationContext) {
        return new NuitonValidatorProvider() {
            @Override
            public <O> NuitonValidator<O> newValidator(NuitonValidatorModel<O> model) {
                return new NuitonValidatorImpl<>(NuitonValidatorProviderFactoryImpl.this::mapping, model);
            }

            @Override
            public Path toPath(NuitonValidatorModelEntry<?> entry, Path rootPath, NuitonValidatorScope scope) {
                Class<?> type = entry.getType();
                String context = entry.getContext();
                return rootPath.resolve(type.getPackageName().replaceAll("\\.", "/")).resolve(String.format("%s%s-%s-validation.json", type.getSimpleName(), (context == null ? "" : "-" + context), scope.name().toLowerCase()));
            }

            @Override
            public NuitonValidatorFileInfo toFileInfo(Path file, Class<?> type, String context, NuitonValidatorScope scope, Set<String> fields) {
                return new NuitonValidatorFileInfoImpl(file, type, context, scope, fields);
            }

            @Override
            public NuitonValidationContext getValidationContext() {
                return validationContext;
            }

            @Override
            public NuitonValidatorModelProvider getModelProvider() {
                return NuitonValidatorProviderFactoryImpl.this.getModelProvider();
            }

            @Override
            public String getName() {
                return NuitonValidatorProviderFactoryImpl.this.getName();
            }
        };
    }

    protected ProjectValidatorMapping mapping() {
        if (mapping == null) {
            try {
                mapping = new ProjectValidatorMappingHelper().readAll();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return mapping;
    }
}

