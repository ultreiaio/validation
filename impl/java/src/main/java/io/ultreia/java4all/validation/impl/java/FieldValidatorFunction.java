package io.ultreia.java4all.validation.impl.java;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidationContext;

import java.util.Objects;
import java.util.function.Function;

/**
 * Created at 02/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
@FunctionalInterface
public interface FieldValidatorFunction<O, C extends NuitonValidationContext, V extends FieldValidator<O, ?>, R> {

    /**
     * Applies this function to the given arguments.
     *
     * @param bean              the first function argument
     * @param validationContext the second function argument
     * @param validator         the third function argument
     * @return the function result
     **/
    R apply(O bean, C validationContext, V validator);

    /**
     * Returns a composed function that first applies this function to
     * its input, and then applies the {@code after} function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param <Z>   the type of output of the {@code after} function, and of the
     *              composed function
     * @param after the function to apply after this function is applied
     * @return a composed function that first applies this function and then
     * applies the {@code after} function
     * @throws NullPointerException if after is null
     */
    default <Z> FieldValidatorFunction<O, C, V, Z> andThen(Function<? super R, ? extends Z> after) {
        Objects.requireNonNull(after);
        return (O o, C c, V v) -> after.apply(apply(o, c, v));
    }
}
