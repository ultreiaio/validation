package io.ultreia.java4all.validation.impl.java;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.validation.api.NuitonValidationContext;

/**
 * Created on 28/01/2024.
 *
 * @param <O> type of object to validate
 * @param <F> type of field to validate
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public interface FieldValidator<O, F> {

    /**
     * @return the field name of the object to validate.
     */
    String getFieldName();

    /**
     * @param object object to validate
     * @return the field of the object to validate
     */
    F getField(O object);

    /**
     * @param object            object to validate
     * @param validationContext to get access to outside world
     * @param messagesCollector to collect messages
     */
    void validate(O object, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector);

    /**
     * @param object            object to validate
     * @param validationContext to get access to outside world
     * @return the validation message
     */
    String getMessage(O object, NuitonValidationContext validationContext);

    default void addMessage(NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector, String messageKey, Object... parameters) {
        messagesCollector.addMessage(getFieldName(), I18n.l(validationContext.getLocale(), messageKey, parameters));
    }
}
