package io.ultreia.java4all.validation.impl.java.io;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.ValidatorField1;
import io.ultreia.java4all.validation.impl.java.definition.ValidatorField2;
import org.junit.Assert;
import org.junit.Test;

import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 26/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class FileValidatorDefinitionBuilderTest {

    static void assertFile(FileValidatorDefinition actual) {
        Map<String, List<FieldValidatorDefinition>> fields = actual.getFields();
        Assert.assertNotNull(fields);
        Assert.assertEquals(2, fields.size());
        Iterator<Map.Entry<String, List<FieldValidatorDefinition>>> iterator = fields.entrySet().iterator();
        assertEntry(iterator.next(), "endDate", 2, new FieldValidatorDefinition("endDate", "endDate > any activity date", ValidatorField1.class, null, null), new FieldValidatorDefinition("endDate", "endDate > any activity date", ValidatorField2.class, null, null));
        assertEntry(iterator.next(), "startDate", 1, new FieldValidatorDefinition("startDate", "startDate < any activity date", ValidatorField1.class, null, null));
    }

    static void assertEntry(Map.Entry<String, List<FieldValidatorDefinition>> entry, String expectedKey, int expectedValidators, FieldValidatorDefinition... exceptedFields) {
        Assert.assertEquals(entry.getKey(), expectedKey);
        Assert.assertEquals(entry.getValue().size(), expectedValidators);
        int index = 0;
        for (FieldValidatorDefinition fieldValidatorDefinition : entry.getValue()) {
            assertField(fieldValidatorDefinition, exceptedFields[index++]);
        }
    }

    static void assertField(FieldValidatorDefinition actual, FieldValidatorDefinition expected) {
        Assert.assertEquals(expected.getFieldName(), actual.getFieldName());
        Assert.assertEquals(expected.getComment(), actual.getComment());
        Assert.assertEquals(expected.getValidator(), actual.getValidator());
        Assert.assertEquals(expected.getParameters(), actual.getParameters());
        Assert.assertEquals(expected.getMessage(), actual.getMessage());
    }

    @Test
    public void build() {

        FileValidatorModelBuilder builder = new FileValidatorModelBuilder();

        URL file = Objects.requireNonNull(getClass().getResource("/io/ultreia/java4all/validation/impl/java/definition/ValidatorBean-update-error-validation.json"));
        FileValidatorDefinition actual = builder.build(file, FileValidatorDefinitionBuilderTest.class, "error", "update");
        Assert.assertNotNull(actual);

        assertFile(actual);
    }

}
