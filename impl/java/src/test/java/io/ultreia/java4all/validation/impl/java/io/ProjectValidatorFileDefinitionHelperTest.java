package io.ultreia.java4all.validation.impl.java.io;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorFileDefinition;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Objects;

/**
 * Created on 27/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class ProjectValidatorFileDefinitionHelperTest {
    static Path getRootPath() throws MalformedURLException, URISyntaxException {
        Path rootPath = Path.of(Objects.requireNonNull(ProjectValidatorFileDefinitionHelper.class.getResource("/io/ultreia/java4all/validation/impl/java/definition/ValidatorBean-update-error-validation.json")).toURI().toURL().getFile());
        while (!rootPath.toFile().getName().equals("test-classes")) {
            rootPath = rootPath.getParent();
        }
        return Objects.requireNonNull(rootPath);
    }

    @Test
    public void build() throws URISyntaxException, IOException {
        Path rootPath = getRootPath();

        ProjectValidatorFileDefinitionHelper helper = new ProjectValidatorFileDefinitionHelper();
        ProjectValidatorFileDefinition actual = helper.build(Objects.requireNonNull(rootPath));
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.getFiles().size(), 1);
        Iterator<String> iterator = actual.getFiles().iterator();
        String actualEntry = iterator.next();
        Assert.assertEquals("/io/ultreia/java4all/validation/impl/java/definition/ValidatorBean-update-error-validation.json", actualEntry);
    }

    @Test
    public void write() throws URISyntaxException, IOException {
        Path rootPath = getRootPath();

        ProjectValidatorFileDefinitionHelper helper = new ProjectValidatorFileDefinitionHelper();
        ProjectValidatorFileDefinition actual = helper.build(Objects.requireNonNull(rootPath));
        Assert.assertNotNull(actual);
        Path target = rootPath.getParent().resolve("surefire-workdir");
        helper.write(actual, target);
        Path file = helper.toLocation(target);
        Assert.assertTrue(Files.exists(file));
        Assert.assertEquals("[\n" +
                                    "  \"/io/ultreia/java4all/validation/impl/java/definition/ValidatorBean-update-error-validation.json\"\n" +
                                    "]", Files.readString(file));
    }

    @Test
    public void readAll() throws IOException {
        ProjectValidatorFileDefinitionHelper helper = new ProjectValidatorFileDefinitionHelper();
        ProjectValidatorFileDefinition actual = helper.readAll();
        Assert.assertNotNull(actual);
        Assert.assertEquals(1, actual.getFiles().size());
        Assert.assertEquals("/io/ultreia/java4all/validation/impl/java/definition/ValidatorBean-update-error-validation.json", actual.getFiles().get(0));
    }

    @Test
    public void read() throws IOException {
        ProjectValidatorFileDefinitionHelper helper = new ProjectValidatorFileDefinitionHelper();
        ProjectValidatorFileDefinition actual = helper.read(Objects.requireNonNull(getClass().getClassLoader().getResource(ProjectValidatorFileDefinitionHelper.LOCATION)));
        Assert.assertNotNull(actual);
        Assert.assertEquals(1, actual.getFiles().size());
        Assert.assertEquals("/io/ultreia/java4all/validation/impl/java/definition/ValidatorBean-update-error-validation.json", actual.getFiles().get(0));
    }
}
