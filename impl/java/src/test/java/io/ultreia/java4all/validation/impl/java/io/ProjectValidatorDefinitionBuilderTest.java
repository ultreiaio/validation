package io.ultreia.java4all.validation.impl.java.io;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.impl.java.definition.FileValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.ValidatorBean;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import static io.ultreia.java4all.validation.impl.java.io.FileValidatorDefinitionBuilderTest.assertFile;

/**
 * Created on 27/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class ProjectValidatorDefinitionBuilderTest {

    @Test
    public void toEntryDefinition() {
        Assert.assertEquals(new FileValidatorEntryDefinition(Object.class, null, "error"), ProjectValidatorDefinitionBuilder.toEntryDefinition("/java/lang/Object-error-validation.json"));
        Assert.assertEquals(new FileValidatorEntryDefinition(Object.class, "context", "error"), ProjectValidatorDefinitionBuilder.toEntryDefinition("/java/lang/Object-context-error-validation.json"));
        Assert.assertEquals(new FileValidatorEntryDefinition(Object.class, "context-yo", "error"), ProjectValidatorDefinitionBuilder.toEntryDefinition("/java/lang/Object-context-yo-error-validation.json"));
    }

    @Test
    public void buildAll() throws IOException {
        ProjectValidatorDefinitionBuilder builder = new ProjectValidatorDefinitionBuilder();
        ProjectValidatorDefinition actual = builder.buildAll();
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.getFilesByEntry().size(), 1);
        Iterator<Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition>> iterator = actual.getFilesByEntry().entrySet().iterator();
        Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition> actualEntry = iterator.next();
        Assert.assertEquals(new FileValidatorEntryDefinition(ValidatorBean.class, "update", "error"), actualEntry.getKey());
        assertFile(actualEntry.getValue());
    }

    @Test
    public void build() throws IOException {
        ProjectValidatorDefinitionBuilder builder = new ProjectValidatorDefinitionBuilder();
        ProjectValidatorDefinition actual = builder.build(Objects.requireNonNull(getClass().getClassLoader().getResource(ProjectValidatorFileDefinitionHelper.LOCATION)));
        Assert.assertNotNull(actual);
        Assert.assertEquals(actual.getFilesByEntry().size(), 1);
        Iterator<Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition>> iterator = actual.getFilesByEntry().entrySet().iterator();
        Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition> actualEntry = iterator.next();
        Assert.assertEquals(new FileValidatorEntryDefinition(ValidatorBean.class, "update", "error"), actualEntry.getKey());
        assertFile(actualEntry.getValue());
    }
}
