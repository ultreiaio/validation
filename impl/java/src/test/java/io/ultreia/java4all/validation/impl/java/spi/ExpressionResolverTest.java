package io.ultreia.java4all.validation.impl.java.spi;

/*-
 * #%L
 * Validation :: Impl :: Java
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;


/**
 * Created at 02/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class ExpressionResolverTest {

    @Test
    public void resolveNoVariable() throws NoSuchMethodException {

        ExpressionResolver resolver = ExpressionResolver.of(new ExpressionResolver.Config("$o", "object", ExpressionResolverTest.class, false));
        Assert.assertEquals("Nada", resolver.resolve("Nada"));
    }

    @Test
    public void resolveOneVariable() throws NoSuchMethodException {

        ExpressionResolver resolver = ExpressionResolver.of(new ExpressionResolver.Config("$o", "object", ExpressionResolverTest.class, false));
        Assert.assertEquals("object.getClass()", resolver.resolve("$o.class"));
        Assert.assertEquals("Node object.getClass() Nada", resolver.resolve("Node $o.class Nada"));
        Assert.assertEquals(" object.getClass()", resolver.resolve(" $o.class"));
        Assert.assertEquals(" object.getClass() ", resolver.resolve(" $o.class "));
        Assert.assertEquals(" object.getName() ", resolver.resolve(" $o.name "));
        Assert.assertEquals(" object.isTest() || object.getName() == \"yo\" ", resolver.resolve(" $o.test || $o.name == \"yo\" "));
        Assert.assertEquals(" object.getContainer().isTest() || object.getName() == \"yo\" ", resolver.resolve(" $o.container.test || $o.name == \"yo\" "));
        Assert.assertEquals(" object.getContainer().isTest() || object.getName().equals(\"yo\") ", resolver.resolve(" $o.container.test || $o.name.equals(\"yo\") "));
        Assert.assertEquals(" !object.getContainer().isTest() || !object.getName().equals(\"yo\") ", resolver.resolve(" !$o.container.test || !$o.name.equals(\"yo\") "));
        Assert.assertEquals(" isEquals(!object.getContainer().isTest(),!object.getName().equals(\"yo\"))", resolver.resolve(" isEquals(!$o.container.test,!$o.name.equals(\"yo\"))"));
        Assert.assertEquals(" isEquals(!object.getContainer().isTest(), !object.getName().equals(\"yo\"))", resolver.resolve(" isEquals(!$o.container.test, !$o.name.equals(\"yo\"))"));
        Assert.assertEquals(" isEquals(!object.getContainer().isTest(), !object.getName().equals(\"yo\") )", resolver.resolve(" isEquals(!$o.container.test, !$o.name.equals(\"yo\") )"));
        Assert.assertEquals(" object.isEquals(!object.getContainer().isTest(), !object.getName().equals(\"yo\") )", resolver.resolve(" $o.isEquals(!$o.container.test, !$o.name.equals(\"yo\") )"));
        Assert.assertEquals(" object.isEquals(object.getContainer().isTest() - !object.getName().equals(\"yo\"))", resolver.resolve(" $o.isEquals($o.container.test - !$o.name.equals(\"yo\"))"));
        resolver = ExpressionResolver.of(new ExpressionResolver.Config("$o", "object", ExpressionResolverTest.class, true));
        Assert.assertEquals(" ((ExpressionResolverTest) object).isEquals(((ExpressionResolverTest) object).getContainer().isTest() - !((ExpressionResolverTest) object).getName().equals(\"yo\"))", resolver.resolve(" $o.isEquals($o.container.test - !$o.name.equals(\"yo\"))"));
    }


    @Test
    public void resolveTowVariable() throws NoSuchMethodException {

        ExpressionResolver resolver = ExpressionResolver.of(new ExpressionResolver.Config("$o", "object", ExpressionResolverTest.class, false), new ExpressionResolver.Config("$B", "b", ExpressionResolverTest.class, true));
        Assert.assertEquals(" object.isEquals(object.getContainer().isTest() - !((ExpressionResolverTest) b).getName().equals(\"yo\"))", resolver.resolve(" $o.isEquals($o.container.test - !$B.name.equals(\"yo\"))"));
    }

    public ExpressionResolverTest getContainer() {
        return this;
    }

    public String getName() {
        return getClass().getName();
    }

    public boolean isTest() {
        return true;
    }
}
