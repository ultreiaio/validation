package io.ultreia.java4all.validation.maven.plugin;

/*-
 * #%L
 * Validation :: Maven plugin
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Strings;
import io.ultreia.java4all.util.ImportManager;
import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.NuitonScopeValidatorImpl;
import io.ultreia.java4all.validation.impl.java.ValidationMessagesCollector;
import io.ultreia.java4all.validation.impl.java.definition.FieldValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.io.ProjectValidatorMappingHelper;
import io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Generated;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static io.ultreia.java4all.validation.impl.java.spi.ExpressionResolver.escapeString;

/**
 * Created at 05/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class NuitonScopeValidatorGenerator {

    public static final String CONTENT =
            "package %1$s;\n\n" +
                    "%2$s" +
                    "\n@Generated(\"io.ultreia.java4all.validation.maven.plugin.GenerateValidatorsMojo\")\n" +
                    "public class %3$s extends NuitonScopeValidatorImpl<%4$s>{\n" +
                    "    public %3$s() {\n" +
                    "        super(%4$s.class , %5$s, new LinkedHashSet<>(List.of(new String[]{\n" +
                    "%6$s\n" +
                    "        })), %7$s);\n" +
                    "    }\n" +
                    "\n" +
                    "    @Override\n" +
                    "    protected void validate(%4$s bean, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {\n" +
                    "%8$s\n" +
                    "    }\n\n" +
                    "%9$s\n" +
                    "}";
    private static final Logger log = LogManager.getLogger(NuitonScopeValidatorGenerator.class);

    private final Set<String> missingGenerators = new TreeSet<>();
    private final FieldValidatorGenerators generators;

    private final Class<? extends NuitonValidationContext> validationContextType;
    private final Map<String, String> classesMapping = new TreeMap<>();

    public NuitonScopeValidatorGenerator(ClassLoader loader, Class<? extends NuitonValidationContext> validationContextType) {
        generators = new FieldValidatorGenerators(loader);
        this.validationContextType = validationContextType;
    }

    public String toSimpleClassName(FileValidatorEntryDefinition key) {
        return String.format("%sValidatorOn%sFor%s", key.getBeanType().getSimpleName(), (key.getContext() == null ? "" : Arrays.stream(key.getContext().split("-")).map(Strings::capitalize).collect(Collectors.joining(""))), Strings.capitalize(key.getScope()));
    }

    public Set<String> getMissingGenerators() {
        return missingGenerators;
    }

    public FieldValidatorGenerators getGenerators() {
        return generators;
    }

    public Map<String, String> getClassesMapping() {
        return classesMapping;
    }

    public Path toLocation(Path rootPath, FileValidatorEntryDefinition key, String fileSimpleName) {
        Path packagePath = rootPath.resolve(key.getBeanType().getPackageName().replaceAll("\\.", "/"));
        return packagePath.resolve(fileSimpleName + ".java");
    }

    public String generate(FileValidatorEntryDefinition key, String filePackageName, String fileSimpleName, FileValidatorDefinition fileValidatorDefinition) throws NoSuchMethodException {
        String fileName = filePackageName + "." + fileSimpleName;
        classesMapping.put(key.getKey(), fileName);
        ImportManager importManager = new ImportManager(filePackageName);
        importManager.addImport(NuitonScopeValidatorImpl.class);
        importManager.addImport(Generated.class);
        importManager.addImport(LinkedHashSet.class);
        importManager.addImport(List.class);
        importManager.addImport(NuitonValidationContext.class);
        importManager.addImport(ValidationMessagesCollector.class);
        importManager.addImport(key.getBeanType());
        Map<String, String> methods = new LinkedHashMap<>();
        int validatorsCount = fileValidatorDefinition.getFields().values().stream().mapToInt(List::size).sum();
        int validatorsIndex = 0;
        for (Map.Entry<String, List<FieldValidatorDefinition>> entry : fileValidatorDefinition.getFields().entrySet()) {
            String fieldName = entry.getKey();
            List<FieldValidatorDefinition> validatorDefinitions = entry.getValue();
            List<String> statements = new LinkedList<>();
            for (FieldValidatorDefinition validatorDefinition : validatorDefinitions) {
                String message = String.format("%1$s[%2$3s/%3$3s] %4$s → `%5$s`", key.getKey(), validatorsIndex + 1, validatorsCount, validatorDefinition.getFieldName(), validatorDefinition.getComment());
                statements.add(String.format("        setValidatorComment(%s);\n", escapeString(message)));

                String supplier = generateFieldGeneratorInstance(generators, importManager, key, fieldName, validatorDefinition);

                statements.add(String.format("        validator(%d, %s).validate(bean, validationContext, messagesCollector);\n", validatorsIndex, supplier));
                validatorsIndex++;
            }
            methods.put("validate" + Strings.capitalize(fieldName), String.join("", statements));
        }

        String simpleName = key.getBeanType().getSimpleName();
        return String.format(CONTENT,
                             filePackageName,
                             importManager.getImportsSection("\n"),
                             fileSimpleName,
                             simpleName,
                             (key.getContext() == null ? "null" : "\"" + key.getContext() + "\""),
                             fileValidatorDefinition.getFields().keySet().stream().map(s -> "                \"" + s + "\"").collect(Collectors.joining(",\n")),
                             validatorsCount,
                             methods.keySet().stream().map(m -> String.format("        %s(bean, validationContext, messagesCollector);", m)).collect(Collectors.joining("\n")),
                             methods.entrySet().stream().map(e -> String.format("    protected void %1$s(%2$s bean, NuitonValidationContext validationContext, ValidationMessagesCollector messagesCollector) {\n" +
                                                                                        "%3$s" +
                                                                                        "    }\n", e.getKey(), simpleName, e.getValue())).collect(Collectors.joining("\n")));
    }

    public void write(String fileContent, Path fileLocation) throws IOException {
        if (Files.notExists(fileLocation.getParent())) {
            Files.createDirectories(fileLocation.getParent());
        }
        Files.write(fileLocation, fileContent.getBytes(StandardCharsets.UTF_8));
    }

    public void writeClassMapping(Path rootPath) throws IOException {
        ProjectValidatorMappingHelper mappingHelper = new ProjectValidatorMappingHelper();

        log.info(String.format("Generate classes mapping at %s", mappingHelper.toLocation(rootPath)));
        mappingHelper.write(classesMapping, rootPath);

    }

    protected String generateFieldGeneratorInstance(FieldValidatorGenerators generators, ImportManager importManager, FileValidatorEntryDefinition key, String fieldName, FieldValidatorDefinition validatorDefinition) throws NoSuchMethodException {
        Class<? extends FieldValidator<?, ?>> validator = validatorDefinition.getValidator();
        Class<?> beanType = key.getBeanType();
        Optional<FieldValidatorGenerator> optionalGenerator = generators.getGenerator(validator);
        if (optionalGenerator.isEmpty()) {
            log.error("No generator for type " + validator.getName());
            String getterName = ExpressionResolver.guessGetterName(beanType, fieldName);
            missingGenerators.add(validator.getName());
            return String.format("/* FIXME %s */() -> new io.ultreia.java4all.validation.impl.java.validator.RequiredFieldValidator<>(\"%s\", %s::%s, null)", validator.getName(), fieldName, beanType.getSimpleName(), getterName);
        }
        return optionalGenerator.get().generate(key, validatorDefinition, validationContextType, importManager);
    }

}


