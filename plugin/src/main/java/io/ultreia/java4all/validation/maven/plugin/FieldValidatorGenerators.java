package io.ultreia.java4all.validation.maven.plugin;

/*-
 * #%L
 * Validation :: Maven plugin
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.impl.java.FieldValidator;
import io.ultreia.java4all.validation.impl.java.spi.FieldValidatorGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader;

/**
 * To use {@link FieldValidatorGenerator}.
 * <p>
 * Created at 01/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
public class FieldValidatorGenerators {
    private static final Logger log = LogManager.getLogger(FieldValidatorGenerators.class);
    private final List<FieldValidatorGenerator> generators;

    public FieldValidatorGenerators(ClassLoader classLoader) {
        log.info("Loading {}", this);
        List<FieldValidatorGenerator> generators = new ArrayList<>();
        for (FieldValidatorGenerator generator : ServiceLoader.load(FieldValidatorGenerator.class, classLoader)) {
            log.info("Found generator {}", generator);
            generators.add(generator);
        }
        this.generators = Collections.unmodifiableList(generators);
        log.info("Loading {} done - found {} generator(s)", this, generators.size());
    }

    public Optional<FieldValidatorGenerator> getGenerator(Class<? extends FieldValidator<?, ?>> validatorType) {
        return generators.stream().filter(g -> g.accept(validatorType)).findFirst();
    }

}
