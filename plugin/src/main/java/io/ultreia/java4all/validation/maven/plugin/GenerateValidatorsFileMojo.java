/*
 * #%L
 * Validation :: Maven plugin
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package io.ultreia.java4all.validation.maven.plugin;

import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorFileDefinition;
import io.ultreia.java4all.validation.impl.java.io.ProjectValidatorFileDefinitionHelper;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Generates {@code files.json} file which contains all validation files detected in {@link #src} directory.
 * <p>
 * Created on 27/01/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
@Mojo(name = "generate-files-json", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class GenerateValidatorsFileMojo extends AbstractMojo {

    /**
     * Directory where validations files are located.
     */
    @Parameter(defaultValue = "${basedir}/src/main/resources", required = true)
    protected File src;
    /**
     * Directory where generate validators.json file.
     */
    @Parameter(defaultValue = "${basedir}/src/main/resources", required = true)
    protected File target;

    @Override
    public void execute() throws MojoExecutionException {

        ProjectValidatorFileDefinitionHelper helper = new ProjectValidatorFileDefinitionHelper();
        try {
            Path path = src.toPath();
            ProjectValidatorFileDefinition model = helper.build(path);
            getLog().info(String.format("%d file(s) detected.", model.getFiles().size()));
            Path location = helper.write(model, target.toPath());
            getLog().info(String.format("files.json generated at %s", location));
        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }
}
