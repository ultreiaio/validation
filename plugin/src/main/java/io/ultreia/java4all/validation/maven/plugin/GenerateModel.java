package io.ultreia.java4all.validation.maven.plugin;

/*-
 * #%L
 * Validation :: Maven plugin
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidatorModel;
import io.ultreia.java4all.validation.api.io.NuitonValidatorModelHelper;
import io.ultreia.java4all.validation.impl.java.NuitonValidatorProviderFactoryImpl;
import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.io.ProjectValidatorDefinitionBuilder;
import io.ultreia.java4all.validation.impl.java.io.ProjectValidatorFileDefinitionHelper;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.List;


/**
 * Created at 06/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
@Mojo(name = "generate-model", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class GenerateModel extends AbstractMojo {
    /**
     * Directory where validators files are located.
     */
    @Parameter(defaultValue = "${basedir}/src/main/resources", required = true)
    protected File resources;
    /**
     * Directory where the validation file is located.
     */
    @Parameter(defaultValue = "${basedir}/src/main/resources", required = true)
    protected File src;
    /**
     * Directory where generate model files.
     */
    @Parameter(defaultValue = "${basedir}/src/main/resources", required = true)
    protected File target;

    @Override
    public void execute() throws MojoExecutionException {

        try (URLClassLoader loader = new URLClassLoader(new URL[]{src.toURI().toURL(), resources.toURI().toURL()}, getClass().getClassLoader())) {

            Path rootPath = target.toPath();
            ProjectValidatorDefinition projectValidatorDefinition = new ProjectValidatorDefinitionBuilder(loader).build(new ProjectValidatorFileDefinitionHelper().toLocation(src.toPath()).toUri().toURL());
            getLog().info(String.format("%d validator(s) detected.", projectValidatorDefinition.getFilesByEntry().size()));
            List<NuitonValidatorModel<?>> models = projectValidatorDefinition.toModels();
            getLog().info(String.format("%s model(s) detected.", models.size()));

            NuitonValidatorModelHelper helper = new NuitonValidatorModelHelper();
            getLog().info(String.format("Generate at %s", helper.toLocation(NuitonValidatorProviderFactoryImpl.PROVIDER_NAME, rootPath)));
            helper.write(NuitonValidatorProviderFactoryImpl.PROVIDER_NAME, models, rootPath);

        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }


}
