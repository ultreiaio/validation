package io.ultreia.java4all.validation.maven.plugin;

/*-
 * #%L
 * Validation :: Maven plugin
 * %%
 * Copyright (C) 2021 - 2024 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.validation.api.NuitonValidationContext;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.definition.FileValidatorEntryDefinition;
import io.ultreia.java4all.validation.impl.java.definition.ProjectValidatorDefinition;
import io.ultreia.java4all.validation.impl.java.io.ProjectValidatorDefinitionBuilder;
import io.ultreia.java4all.validation.impl.java.io.ProjectValidatorFileDefinitionHelper;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.Map;
import java.util.Set;

/**
 * Generates validators as Java files.
 * <p>
 * Created at 01/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.0.0
 */
@Mojo(name = "generate-validators", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class GenerateValidatorsMojo extends AbstractMojo {

    /**
     * Directory where validators files are located.
     */
    @Parameter(defaultValue = "${basedir}/src/main/resources", required = true)
    protected File resources;
    /**
     * Directory where the validators files is located.
     */
    @Parameter(defaultValue = "${basedir}/src/main/resources", required = true)
    protected File src;
    /**
     * Directory where generate java validator files.
     */
    @Parameter(defaultValue = "${basedir}/target/generated-sources/java", required = true)
    protected File target;
    /**
     * Root directory where generate the {@code mapping.json}.
     * <p>
     * The actual file will be generated to {@code ${mappingTarget}/META-INF/validation/mapping.json}
     */
    @Parameter(defaultValue = "${basedir}/src/main/resources", required = true)
    protected File mappingTarget;
    /**
     * Validation context type.
     */
    @Parameter(required = true)
    protected String validationContextType;
    /**
     * Maven project.
     */
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    @Override
    public void execute() throws MojoExecutionException {

        try (URLClassLoader loader = new URLClassLoader(new URL[]{src.toURI().toURL(), resources.toURI().toURL()}, getClass().getClassLoader())) {

            @SuppressWarnings("unchecked") NuitonScopeValidatorGenerator generator = new NuitonScopeValidatorGenerator(loader, (Class<? extends NuitonValidationContext>) loader.loadClass(validationContextType));

            Path rootPath = target.toPath();
            ProjectValidatorDefinition projectValidatorDefinition = new ProjectValidatorDefinitionBuilder(loader).build(new ProjectValidatorFileDefinitionHelper().toLocation(src.toPath()).toUri().toURL());
            getLog().info(String.format("%d validator(s) detected.", projectValidatorDefinition.getFilesByEntry().size()));
            getLog().info(String.format("Generate at %s", rootPath));
            for (Map.Entry<FileValidatorEntryDefinition, FileValidatorDefinition> entry : projectValidatorDefinition.getFilesByEntry().entrySet()) {

                FileValidatorEntryDefinition key = entry.getKey();
                String filePackageName = key.getBeanType().getPackageName();
                String fileSimpleName = generator.toSimpleClassName(key);

                FileValidatorDefinition fileValidatorDefinition = entry.getValue();

                Path fileLocation = generator.toLocation(rootPath, key, fileSimpleName);
                String fileContent = generator.generate(key, filePackageName, fileSimpleName, fileValidatorDefinition);
                getLog().debug(String.format("Generate %s.%s at %s", filePackageName, fileSimpleName, fileLocation));
                generator.write(fileContent, fileLocation);
            }

            Map<String, String> classesMapping = generator.getClassesMapping();
            getLog().info(String.format("%s validator(s) generated.", classesMapping.size()));

            generator.writeClassMapping(mappingTarget.toPath());

            Set<String> missingGenerators = generator.getMissingGenerators();
            if (!missingGenerators.isEmpty()) {
                throw new MojoExecutionException(String.format("There is %d missing generator(s):\n * %s", missingGenerators.size(), String.join("\n * ", missingGenerators)));
            }
            project.addCompileSourceRoot(target.toString());
        } catch (IOException | NoSuchMethodException | ClassNotFoundException e) {
            throw new MojoExecutionException(e);
        }
    }


}
